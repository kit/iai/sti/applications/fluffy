import {defineConfig} from "vite";
import react from "@vitejs/plugin-react";
import eslint from "vite-plugin-eslint";

const proxy = {
    target: 'http://localhost:8000',
    changeOrigin: true,
    secure: true
};

export default defineConfig({
    plugins: [
        react({
            babel: {
                plugins: ["@emotion/babel-plugin"],
            },
        }),
        eslint(),
    ],
    server: {
        port: 3000,
        host: true,
        open: true,
        proxy: {
            '/api': proxy,
            '/is-responding': proxy,
        }
    },
    build: {
        outDir: "./build",
        assetsDir: "./static"
    },
});
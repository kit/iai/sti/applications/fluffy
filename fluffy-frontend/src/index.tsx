import React from 'react';
import {createRoot} from 'react-dom/client';
import {QueryClientProvider} from '@tanstack/react-query';
import {ReactQueryDevtools} from '@tanstack/react-query-devtools'
import {BrowserRouter} from 'react-router-dom';
import {IconContext} from 'react-icons';
import App from './App';
import OidcProvider from "./OidcProvider";
import {StoreProvider} from "./state/context";
import {queryClient} from "./queryClient";

import './index.css';
import '@szhsin/react-menu/dist/core.css';
import '@szhsin/react-menu/dist/transitions/slide.css';
import axios from "axios";

axios.defaults.withCredentials = true;

const root = createRoot(document.getElementById('root') as HTMLElement);
root.render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
            <OidcProvider>
                <StoreProvider>
                    <BrowserRouter>
                        <IconContext.Provider value={{ style: { transform: 'translate(0, 1px)' } }}>
                            <App/>
                        </IconContext.Provider>
                    </BrowserRouter>
                </StoreProvider>
            </OidcProvider>
            <ReactQueryDevtools/>
        </QueryClientProvider>
    </React.StrictMode>,
);

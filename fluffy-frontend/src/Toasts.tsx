import {Toaster} from 'react-hot-toast';
import { ImCheckmark } from "react-icons/im";
import { PiWarningFill } from "react-icons/pi";

const Toasts = () =>
    <Toaster
        toastOptions={{
            success: {
                icon: <ImCheckmark size={40} color={"darkgreen"}/>,
                style: {
                    backgroundColor: "#cfc"
                }
            },
            error: {
                icon: <PiWarningFill size={30} color={"darkred"}/>,
                style: {
                    backgroundColor: "#fcc"
                }
            },
            style: {
                paddingLeft: "1rem"
            }
        }}
    />;

export default Toasts;

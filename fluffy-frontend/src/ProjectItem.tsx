import React from "react";
import {useDeleteProject, useProjectDetails} from "./hooks/projects.hooks";
import {Badge, ProjectDetailsEntryFrame, ProjectEntryFrame, ProjectsListItem} from "./ProjectsStyles";
import LoadingSpinner, {LoadingSpinnerOverlay} from "./utils/LoadingSpinner";
import {AiOutlineDelete} from "react-icons/ai";
import {Project, ProjectOverview, UserRoleInfo} from "./model";
import ProjectMembers from "./ProjectMembers";
import {Warning} from "./utils/Components";
import ProjectRoles from "./ProjectRoles";


type ProjectItemProps = {
    project: ProjectOverview,
    opened: boolean,
    onOpen?: (o: boolean) => void,
};

export const ProjectItem = ({project, opened = false, onOpen}: ProjectItemProps) => {
    const [open, setOpen] = React.useState<boolean>(opened);
    const {deleteProject, isDeleting} = useDeleteProject(project.id);

    // TODO - ask about: shouldn't managers be able to edit name/roottopic of a project?
    // -> was never discussed again, but specified in an issue very early on

    const onClick = () => {
        if (onOpen) {
            onOpen(!open);
        }
        setOpen(it => !it);
    };

    const canDelete = open && project.roleCount === 0;

    return (
        <ProjectEntryFrame style={{opacity: isDeleting ? 0.3 : 1}}>
            <LoadingSpinnerOverlay active={isDeleting}/>
            <ProjectsListItem
                onClick={onClick}
                highlight={open}
            >
                <div>
                    {project.name}
                    <UserRoleBadge userRole={project}/>
                </div>
                <div>
                    <span style={{opacity: 0.5, fontSize: 'small'}}>
                        Root Topic:
                    </span>
                    <span style={{marginLeft: "0.5rem"}}>
                        {project.roottopic}/...
                    </span>
                </div>
                <button style={{visibility: canDelete ? 'visible' : 'hidden'}} onClick={deleteProject}>
                    <AiOutlineDelete/>
                </button>
            </ProjectsListItem>
            <ProjectDetailsEntry
                project={project}
                open={open}
            />
        </ProjectEntryFrame>
    );
};

const UserRoleBadge = ({userRole}: {userRole: UserRoleInfo}) => {
    const { isMember, isManager, isSuperUser } = userRole;
    if (!isMember && !isManager && !isSuperUser) {
        return null;
    }
    return <div>
        <Badge>
            {
                isSuperUser ? 'Superuser' :
                    isManager ? 'Manager' :
                        isMember ? 'Member' :
                            ''
            }
        </Badge>
    </div>;
};

const ProjectDetailsEntry = ({project, open}: { project: ProjectOverview, open: boolean }) => {
    if (!open) {
        return null;
    }
    return (
        <ProjectDetailsEntryFrame>
            <ProjectMembers
                project={project!}
                visible={project.canEditMembers}
            />
            <ProjectDetails
                project={project}
            />
        </ProjectDetailsEntryFrame>
    );
};

const ProjectDetails = ({project}: { project: Project }) => {
    const {data, isLoading, isError, error} = useProjectDetails(project.id);

    if (isLoading) {
        return <LoadingSpinner/>;
    }

    if (isError || !data) {
        console.warn("Error in retrieving project details:", error);
        return (
            <Warning>
                Error fetching project.
            </Warning>
        );
    }

    return <ProjectRoles details={data!.data}/>;
};

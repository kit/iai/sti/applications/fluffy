import {DetailedProject, Device, Role} from "./model";
import {useDeviceMutations} from "./hooks/mqtt.hooks";
import React, {useState} from "react";
import {
    DashedList,
    DashedListItemRow,
    FlexColumn,
    FlexRow,
    LeftAlign, LinkStyle, PlusButton,
    SideColumn, SmallHint,
    ThreeDotMenu,
    WithSideColumn
} from "./utils/Components";
import {MenuItem} from "@szhsin/react-menu";
import {ImCross} from "react-icons/im";
import {GrKey} from "react-icons/gr";
import {AiOutlineDelete} from "react-icons/ai";
import {LoadingSpinnerOverlay} from "./utils/LoadingSpinner";
import {SelectOption, whenEnterIf} from "./utils/formUtils";
import toast from "react-hot-toast";
import Select from "./utils/Select";
import {inOrder} from "./utils/ordering";


export const DevicesForRole = ({role, project}: { role: Role, project: DetailedProject }) => {
    const [createdPasswords, setCreatedPasswords] = React.useState<Record<number, string>>({});

    return (
        <WithSideColumn>
            <ListDevices
                role={role}
                project={project}
                createdPasswords={createdPasswords}
                setCreatedPasswords={setCreatedPasswords}
            />
            <AddDevices
                role={role}
                project={project}
                setCreatedPasswords={setCreatedPasswords}
            />
        </WithSideColumn>
    )
};
const AddDevices = ({role, project, setCreatedPasswords}: {
    role: Role,
    project: DetailedProject,
    setCreatedPasswords: React.Dispatch<React.SetStateAction<Record<number, string>>>,
}) => {
    const {postDevice, addDeviceToRole, isPatching} = useDeviceMutations(project);
    const [newDeviceName, setNewDeviceName] = React.useState<string>("");
    const [selectedOption, setSelectedOption] = React.useState<SelectOption<number> | undefined>();

    const onPostDevice = () => {
        postDevice(newDeviceName, role.id)
            .then((response: any) => {
                setCreatedPasswords(state => ({
                    ...state,
                    [response.data.id]: response.data.password
                }));
                toast.success(
                    `Device "${response.data.name}" was created with the following password:\n${response.data.password}\n\n... you need this password to connect the device with the HiveMQ database, if you lose it it will have to be reset.`,
                    {
                        duration: Number.MAX_SAFE_INTEGER,
                    }
                );
                setNewDeviceName("");
            })
            .catch(console.warn);
    };

    const remainingOptions = React.useMemo(() =>
            project.devices
                .filter(device => !role.devices.includes(device.id))
                .map((device: Device) => ({
                    value: device.id,
                    label: device.name
                } as SelectOption<number>))
        , [project.devices, role.devices]);

    const remainingDevicesHint = project.devices.length === 0
        ? "This project has no devices defined yet."
        : remainingOptions.length === 0
            ? "This role already uses all available devices."
            : remainingOptions.length === 1
                ? `1 device remaining in "${project.name}".`
                : `${remainingOptions.length} devices remaining in "${project.name}".`;

    return (
        <SideColumn>
            <LoadingSpinnerOverlay
                active={isPatching}
            />
            <FlexRow style={{justifyContent: "flex-end"}}>
                <div style={{fontSize: "smaller"}}>
                    Create new device:
                </div>
                <input
                    placeholder="New Device Name..."
                    value={newDeviceName}
                    onChange={event => setNewDeviceName(event.target.value)}
                    onKeyDown={whenEnterIf(!!newDeviceName, onPostDevice)}
                    onFocus={e => e.target.select()}
                    style={{marginRight: 0, flex: 1}}
                />
                <PlusButton
                    onClick={onPostDevice}
                    disabled={!newDeviceName}
                    style={{marginLeft: 0}}
                />
            </FlexRow>
            <FlexRow>
                <div>Assign existing device:</div>
                <Select
                    options={remainingOptions}
                    onChange={(option: any) =>
                        setSelectedOption(option)
                    }
                    value={selectedOption}
                    placeholder={remainingDevicesHint}
                    isDisabled={remainingOptions.length === 0}
                    controlShouldRenderValue={!!selectedOption}
                />
                <PlusButton
                    onClick={() => {
                        const deviceName = selectedOption!.value;
                        setSelectedOption(undefined);
                        addDeviceToRole(deviceName, role.id);
                    }}
                    disabled = {!selectedOption}
                />
            </FlexRow>
        </SideColumn>
    );
};

const ListDevices = ({role, project, createdPasswords, setCreatedPasswords}: {
    role: Role,
    project: DetailedProject,
    createdPasswords: Record<number, string>,
    setCreatedPasswords: React.Dispatch<React.SetStateAction<Record<number, string>>>,
}) => {
    const {deleteDevice, removeDeviceFromRole, resetDevicePassword, isPending} = useDeviceMutations(project);
    const [devicePending, setDevicePending] = React.useState<number | undefined>();
    const [hover, setHover] = useState<number | undefined>();

    React.useEffect(() => {
        setDevicePending(undefined);
        // eslint-disable-next-line
    }, [role, project]);

    const devices = React.useMemo(() =>
        inOrder(
            role.devices
                .map(deviceId =>
                    project.devices.find(d => d.id === deviceId)
                    ?? {
                            id: -1,
                            name: `Invalid device with id ${deviceId}`,
                        }
                )
        ) as Device[]
    , [role.devices, project.devices]);

    const confirmDelete = (device: Device) => () => {
        if (!window.confirm(
            `Are you sure to delete device "${device.name}" irreversibly?`
        )) {
            return;
        }
        setDevicePending(device.id);
        deleteDevice(device.id)
            .then(() => {
                toast.success(`Device "${device.name}" was successfully deleted from project "${project.name}".`);
            });
    };

    const confirmResetPassword = (device: Device) => () => {
        if (!window.confirm(
            "If you reset the password, you will have to update it in the already connected device(s). \n\n" +
            "Do you want to continue?"
        )) {
            return;
        }
        setDevicePending(device.id);
        resetDevicePassword(device.id)
            .then((response: {data: string}) => {
                setCreatedPasswords(state => ({
                    ...state,
                    [device.id]: response.data,
                }));
                toast.success(
                    `Device "${device.name}" got a new password.\n` +
                    "See the corresponding device row.\n\n" +
                    "Store this somewhere safe, it is lost when this browser / session is closed.",
                    {
                        duration: 10000
                    }
                );
            })
            .catch((err) => {
                toast.error(`Could not delete device: ${err}`);
                console.warn(err);
            })
            .finally(() => {
                setDevicePending(undefined);
            });
    };

    if (devices.length === 0) {
        return (
            <FlexColumn>
                <LeftAlign>
                    Devices
                </LeftAlign>
                <SmallHint>
                    This role has no devices assigned.
                </SmallHint>
            </FlexColumn>
        );
    }

    return (
        <FlexColumn>
            <LoadingSpinnerOverlay active={isPending}/>
            <LeftAlign>
                Devices
            </LeftAlign>
            <DashedList>
                {devices.map((d: Device) =>
                    <DashedListItemRow
                        key={d.id}
                        onMouseEnter={() => setHover(d.id)}
                        onMouseLeave={() => setHover(undefined)}
                        style={{
                            opacity: devicePending === d.id ? 0.5 : 1,
                            pointerEvents: devicePending === d.id ? "none" : undefined,
                        }}
                    >
                        <ThreeDotMenu
                            tooltip={"Device actions"}
                            onlyHint={hover !== d.id}
                        >
                            <MenuItem onClick={() => {
                                setDevicePending(d.id);
                                removeDeviceFromRole(d.id, role.id);
                            }}>
                                <ImCross style={{marginRight: '0.6rem'}}/>
                                Remove from this Role
                            </MenuItem>
                            <MenuItem onClick={confirmResetPassword(d)}>
                                <GrKey style={{marginRight: '0.6rem'}}/>
                                Reset Device Password
                                {" "}
                                <span style={{opacity: 0.3, fontSize: 'small'}}>
                                    (no undo!)
                                </span>
                            </MenuItem>
                            <MenuItem onClick={confirmDelete(d)}>
                                <AiOutlineDelete style={{marginRight: '0.6rem'}}/>
                                Delete Device
                                {" "}
                                <span style={{opacity: 0.3, fontSize: 'small'}}>
                                    (no undo!)
                                </span>
                            </MenuItem>
                        </ThreeDotMenu>
                        <div style={{flex: 3}}>
                            {d.name}
                        </div>
                        <div style={{flex: 2}}>
                            <JustCreatedPasswords
                                createdPasswords={createdPasswords}
                                onRemove={(deviceId: number) => setCreatedPasswords(state => ({
                                    ...state,
                                    [deviceId]: "",
                                }))}
                                device={d}
                            />
                        </div>
                    </DashedListItemRow>
                )}
            </DashedList>
        </FlexColumn>
    );
};

const JustCreatedPasswords = ({device, createdPasswords, onRemove}: {
    device: Device,
    createdPasswords: Record<number, string>,
    onRemove: (deviceId: number) => void,
}) => {
    const password = createdPasswords[device.id];

    if (!password) {
        return null;
    }

    return (
        <FlexRow style={{fontSize: 'small'}}>
            got new password:{" "}
            <LinkStyle onClick={() => toast(`Password for ${device.name}: ${password}`)}>
                Show
            </LinkStyle>
            <LinkStyle onClick={() => {
                navigator.clipboard.writeText(password);
                toast(`Password for device "${device.name}" copied to clipboard.`);
            }}>
                Copy
            </LinkStyle>
            <LinkStyle onClick={() => onRemove(device.id)}>
                Forget
            </LinkStyle>
        </FlexRow>
    );
};

import React from 'react';
import styled from '@emotion/styled';
import {Warning} from './utils/Components';
import {useAuth} from "react-oidc-context";
import toast from "react-hot-toast";

const LoginManager = ({children}: {children: React.ReactElement}) => {
    const oicdAuth = useAuth();
    const triedOneSignin = React.useRef(false);

    React.useEffect(() => {
        if (triedOneSignin.current) {
            return;
        }
        oicdAuth.signinSilent()
            .catch(err => {
                console.warn("SigninSilent failed", err.message);
            })
            .finally(() => {
                triedOneSignin.current = true;
            });

        return oicdAuth.events.addAccessTokenExpiring(() => {
            toast('OICD Auth Token is expiring.');
            oicdAuth.signinSilent();
        })

    // eslint-disable-next-line
    }, [oicdAuth.signinSilent, oicdAuth.events]);

    const debug = async (event: any) => {
        event.preventDefault();
        await oicdAuth.removeUser();
        await oicdAuth.revokeTokens();
        await oicdAuth.clearStaleState();
        console.log(oicdAuth);
    };

    if (oicdAuth.isAuthenticated) {
        return children;
    }

    return (
        <LoginBlock>
            <OidcButton
                onClick = {() => oicdAuth.signinRedirect()}
                onContextMenu = {debug}
            >
                Sign in via KIT OpenID Connect
            </OidcButton>
            <Warning>
                <div>
                    You need to sign in before you can continue
                </div>
            </Warning>
        </LoginBlock>
    );
};

export default LoginManager;

export const LoginBlock = styled.div`
    background: #000a;
    display: flex;
    flex-direction: column;
`;

const OidcButton = styled.button`
  background-color: lightseagreen;
  color: white;
  margin: 1rem;
  transition: 0.2s;
  padding: 1rem;
  
  &:hover {
    background-color: palegreen;
    color: black;
  }
`;

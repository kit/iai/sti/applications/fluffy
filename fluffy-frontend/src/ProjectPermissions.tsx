import {KeyOfPermission, PERMISSION_KEYS, RolePermissions} from "./model";
import React, {CSSProperties} from "react";
import {usePermissionMutations} from "./hooks/mqtt.hooks";
import {FlexRow, SmallHint, SquareButton} from "./utils/Components";
import {ImFloppyDisk, ImUndo} from "react-icons/im";
import {AiOutlineDelete} from "react-icons/ai";
import styled from "@emotion/styled";


const permissionLabel = Object.freeze({
    allow_publish: 'Publish',
    allow_subscribe: 'Subscribe',
    allow_qos0: 'QoS 0',
    allow_qos1: 'QoS 1',
    allow_qos2: 'QoS 2',
    allow_retained: 'Retained',
    allow_shared_subscriptions: 'Shared Subscriptions'
});

export const PermissionTableFor = ({projectId, permissions, onClickDelete, disabled}: {
    projectId: number,
    permissions: RolePermissions[],
    onClickDelete: (p: RolePermissions) => void,
    disabled?: boolean,
}) => {

    if (permissions?.length === 0) {
        return (
            <SmallHint>
                This role does not have any topics yet.
            </SmallHint>
        );
    }

    return (
        <PermissionTable disabled={disabled}>
            <thead>
            <tr style={{fontSize: "smaller"}}>
                <th style={{width: '30%'}}>Topic</th>
                {
                    PERMISSION_KEYS
                        .filter(key => key !== 'allow_shared_subscriptions')
                        .map(key =>
                            <th key={key}
                                style={{width: '6%', whiteSpace: "nowrap"}}>
                                {permissionLabel[key]}
                            </th>
                        )
                }
                {
                    <th style={{width: '10%', paddingLeft: "0.5rem"}} colSpan={2}>
                        {permissionLabel['allow_shared_subscriptions']}
                    </th>
                }
                <th/>
            </tr>
            </thead>
            <tbody>
            {
                permissions.map((perm: RolePermissions) =>
                    <PermissionRowFor
                        permissions={perm}
                        projectId={projectId}
                        key={perm.id}
                        onClickDelete={() => onClickDelete(perm)}
                    />
                )
            }
            </tbody>
        </PermissionTable>
    );
};

const PermissionTable = styled.table<{disabled?: boolean}>`
  font-size: smaller;
  border-collapse: collapse;
  
  ${props => props.disabled ? `
    pointer-events: none;
    opacity: 0.3;
  ` : ''};
    
  & th {
    font-weight: normal;
  }
    
  & td:not(:first-of-type) {
    text-align: center;
  }
`;

const PermissionRowFor = ({permissions: storedPermissions, projectId, onClickDelete}: {
    permissions: RolePermissions,
    projectId: number,
    onClickDelete: () => void,
}) => {
    const [touched, setTouched] = React.useState<boolean>(false);
    const [permissions, setPermissions] = React.useState<RolePermissions>(storedPermissions);
    const {patchPermissions, isPatching} = usePermissionMutations(projectId);

    const togglePermissions = (key: KeyOfPermission) => {
        setPermissions(state => ({...state, [key]: !state[key]}));
        setTouched(true);
    };

    const onReset = () => {
        setPermissions(storedPermissions);
        setTouched(false);
    };

    const onSubmit = () => {
        patchPermissions(permissions)
            .then(() => setTouched(false))
            .catch(console.warn);
    };

    React.useEffect(() => {
        const warn = (event: BeforeUnloadEvent) => {
            if (!touched) {
                return undefined;
            }
            event.returnValue = `Permissions for topic "${permissions.topic}" have unsaved changes. Leave?`;
            return event.returnValue;
        }
        window.addEventListener('beforeunload', warn);
        return () => window.removeEventListener('beforeunload', warn);
    }, [touched, permissions.topic]);

    const loadingStyle = (isPatching ? {
        opacity: 0.6,
        cursor: 'wait',
    } : {}) as CSSProperties;

    return (
        <tr style={{
            fontSize: 'smaller',
            backgroundColor: touched ? '#a112' : undefined,
            ...loadingStyle,
        }}>
            <td style={{fontSize: '1.3em', fontWeight: "bold", whiteSpace: "nowrap"}}>
                {permissions.topic}
                <span style={{
                    visibility: touched ? "visible" : "hidden",
                    color: "#fbb",
                    fontWeight: "normal",
                    fontSize: 'smaller'
                }}>
                    {" "}(unsaved)
                </span>
            </td>
            {
                PERMISSION_KEYS.map(key =>
                    <PermissionCheckbox
                        permissions={permissions}
                        togglePermissions={togglePermissions}
                        name={key}
                        title={permissionLabel[key]}
                        disabled={isPatching}
                        key={key}
                    />
                )
            }
            <td>
                <input
                    placeholder={"group name..."}
                    value={permissions.shared_group}
                    onChange={event => setPermissions(state =>
                        ({...state, shared_group: event.target.value}))}
                    disabled={isPatching || !permissions.allow_shared_subscriptions}
                    style={{width: '4vw', flex: 2, fontSize: 'small', padding: '0.5rem'}}
                />
            </td>
            <td>
                <FlexRow>
                    <SquareButton
                        disabled={!touched || isPatching}
                        onClick={onSubmit}
                        title="Save changes"
                    >
                        <ImFloppyDisk/>
                    </SquareButton>
                    <SquareButton
                        disabled={!touched || isPatching}
                        onClick={onReset}
                        title="Discard changes"
                    >
                        <ImUndo/>
                    </SquareButton>
                    <SquareButton
                        disabled={touched}
                        onClick={onClickDelete}
                        title="Delete this topic (no undo!)"
                    >
                        <AiOutlineDelete/>
                    </SquareButton>
                </FlexRow>
            </td>
        </tr>
    );
};

type CheckboxProps = {
    permissions: RolePermissions,
    togglePermissions: (name: KeyOfPermission) => void,
    name: KeyOfPermission,
    title?: string,
    disabled?: boolean,
}

const PermissionCheckbox = ({permissions, togglePermissions, name, title, disabled}: CheckboxProps) => {
    return (
        <td>
            <input
                type="checkbox"
                id={name}
                checked={permissions[name] ?? false}
                onChange={() => togglePermissions(name)}
                disabled={disabled || permissions[name] === undefined}
                title={title}
            />
        </td>
    );
};

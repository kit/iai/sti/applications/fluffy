import InlineForm from "./utils/InlineForm";
import {whenEnter, whenEnterIf} from "./utils/formUtils";
import {useInputState} from "./hooks/hooks";
import {usePermissionMutations, useRoleMutations} from "./hooks/mqtt.hooks";
import {
    DashedList,
    DashedListItem,
    FlexRow,
    LeftAlign,
    SideColumn,
    PlusButton,
    WithSideColumn, FlexColumn
} from "./utils/Components";
import styled from "@emotion/styled";
import {LoadingSpinnerOverlay} from "./utils/LoadingSpinner";
import React from "react";
import {DetailedProject, Role, RolePermissions} from "./model";
import {AiOutlineDelete} from "react-icons/ai";
import toast from "react-hot-toast";
import {PermissionTableFor} from "./ProjectPermissions";
import {DevicesForRole} from "./ProjectDevices";


const ProjectRoles = ({details}: {details: DetailedProject}) => {

    const sortedRoles = React.useMemo(() =>
        details.roles.toSorted((a, b) =>
            a.name.toLowerCase() < b.name.toLowerCase() ? -1 : +1
        )
    , [details.roles]);

    if (!details.roles.length) {
        return <>
            <AddProjectRole project={details}/>
            <div>
                This project does not have any roles yet.
            </div>
        </>;
    }

    return <>
        <FlexRow>
            <h3 style={{margin: "0.5rem"}}>
                Roles
            </h3>
            <div style={{fontSize: 'smaller', opacity: 0.6}}>
                This project has {details.roles.length} role{details.roles.length === 1 ? '' : 's'}.
            </div>
        </FlexRow>
        <AddProjectRole project={details}/>
        <DashedList>
            {
                sortedRoles.map((role: Role) =>
                    <ProjectRoleItem
                        role={role}
                        project={details}
                        key = {role.id}
                    />
                )
            }
        </DashedList>
    </>;
};

export default ProjectRoles;


const ProjectRoleItem = ({role, project}: {role: Role, project: DetailedProject}) => {
    const { deleteRole, isDeleting } = useRoleMutations(project.id);
    const { postPermissions, deletePermissions, isPending: permissionsPending } = usePermissionMutations(project.id);
    const [removedPermissions, setRemovedPermissions] = React.useState<number[]>([]);
    const [roleRemoved, setRoleRemoved] = React.useState<boolean>(false);
    const [newTopic, setNewTopic] = React.useState<string>("");

    React.useEffect(() => {
        setRemovedPermissions([]);
        // eslint-disable-next-line
    }, [project]);

    const visiblePermissions = React.useMemo(() =>
        role.permissions.filter(permission => !removedPermissions.includes(permission.id!))
        // eslint-disable-next-line
    , [removedPermissions, role.permissions, project]);

    const allPermissions = project.roles.flatMap(role => role.permissions);

    const onPostNewTopic = async () => {
        const fullTopic = [project.roottopic, newTopic].join('/');
        const conflict = allPermissions.find(p => p.topic === fullTopic);
        if (conflict) {
            const roleName = project.roles.find(r => r.id === conflict.role)?.name;
            toast.error(`Topic "${fullTopic}" already exists in role "${roleName}"`);
            return;
        }
        postPermissions({ topic: newTopic, role: role.id })
            .then(() => {
                setNewTopic("");
                toast.success(`Topic "${fullTopic}" successfully created.`);
            })
            .catch(console.warn);
    };

    const onClickDelete = (permissions: RolePermissions) => {
        if (!window.confirm(`Are you sure to delete the topic "${permissions.topic}"? (no undo!)`)) {
            return;
        }
        deletePermissions(permissions.id)
            .then(() => setRemovedPermissions(state => [...state, permissions.id!]))
            .catch(console.warn);
    };

    if (roleRemoved) {
        return null;
    }

    return (
        <DashedListItem style={{
            opacity: isDeleting ? 0.5 : 1,
            pointerEvents: isDeleting ? "none" : undefined,
            padding: "0.2rem"
        }}>
            <RoleHeader>
                <RoleTitle id={role.id?.toString()}>
                    <span>{role.name}:{" "}</span>
                </RoleTitle>
                <span style={{opacity: 0.5, marginTop: "0.15rem"}}>
                    {role.description}
                </span>
                <DeleteRoleButton
                    onSubmit = {() =>
                        deleteRole(role.id)
                        .then(() => setRoleRemoved(true))
                    }
                    role={role}
                    isNotEmpty={role.devices.length > 0 || visiblePermissions.length > 0}
                />
            </RoleHeader>
            <div style={{marginLeft: "2rem"}}>
                <WithSideColumn>
                    <FlexColumn>
                        <LeftAlign>
                            Topics & Permissions
                        </LeftAlign>
                        <LoadingSpinnerOverlay
                            active={permissionsPending}
                        />
                        <PermissionTableFor
                            projectId = {project.id}
                            permissions = {visiblePermissions}
                            onClickDelete = {onClickDelete}
                            disabled = {permissionsPending}
                        />
                    </FlexColumn>
                    <SideColumn>
                        <FlexRow>
                            <div style={{fontSize: "smaller"}}>
                                Add new subtopic under <b>{project.roottopic}/</b> :
                            </div>
                            <input
                                value={newTopic}
                                onChange={e => setNewTopic(e.target.value)}
                                onKeyDown={whenEnterIf(!!newTopic, onPostNewTopic)}
                                onFocus={e => e.target.select()}
                                placeholder="MQTT subtopic..."
                                style={{flex: 1}}
                            />
                            <PlusButton
                                disabled={!newTopic}
                                onClick={onPostNewTopic}
                            />
                        </FlexRow>
                    </SideColumn>
                </WithSideColumn>
                <DevicesForRole
                    role = {role}
                    project = {project}
                />
            </div>
        </DashedListItem>
    );
};


const RoleHeader = styled.div`
  display: flex;
  align-items: flex-start;
  gap: 0.5rem;
  padding-right: 0.7rem;

  & *:last-child {
    margin-left: auto;
  }
`;

const RoleTitle = styled.div`
  margin-top: -0.15em;
  font-size: large;
  padding: 0.2rem;
`;


const AddProjectRole = ({project}: {project: DetailedProject}) => {
    const [roleName, updateRoleName, resetName] = useInputState();
    const [description, updateDescription, resetDescription] = useInputState();
    const {postRole, isPosting} = useRoleMutations(project.id);

    const submit = async () => {
        await postRole(roleName, description);
        toast.success(`Role "${roleName} added to Project "${project.name}"`);
        resetName();
        resetDescription();
        // navigate(`#${response.data.id - 1}`, {replace: false}); // doesn't work yet
    };

    return (
        <div style={{paddingLeft: "0.5rem", paddingRight: "0.1rem"}}>
            <InlineForm pending={isPosting}>
                <LoadingSpinnerOverlay active={isPosting}/>
                <div>Add role:</div>
                <input
                    value = {roleName}
                    onChange = {updateRoleName}
                    onKeyDown = {whenEnter(submit)}
                    placeholder = "role name"
                    disabled = {isPosting}
                />
                <input
                    value = {description}
                    onChange = {updateDescription}
                    onKeyDown = {whenEnter(submit)}
                    placeholder = "description"
                    disabled = {isPosting}
                />
                <PlusButton
                    onClick = {submit}
                    disabled = {!roleName || !description || isPosting}
                    title = {!roleName || !description
                        ? 'You need to enter both name and description to add role'
                        : 'Add role'
                    }
                />
            </InlineForm>
        </div>
    );
};

const DeleteRoleButton = ({onSubmit, role, isNotEmpty}: {
    onSubmit: () => Promise<void>,
    role: Role,
    isNotEmpty: boolean
}) => {
    const submit = () => {
        const message = isNotEmpty
            ? `The role "${role.name}" has still permissions and/or devices assigned. \n\nAre you sure to delete it irreversibly?`
            : `This will irreversibly delete the role "${role.name}". Are you sure?`;
        if (!window.confirm(message)) {
            return;
        }
        onSubmit();
    }
    return (
        <button onClick={submit}>
            <AiOutlineDelete/>
        </button>
    );
};

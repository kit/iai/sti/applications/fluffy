import React from 'react';
import Projects from './Projects';
import LoginManager from './LoginManager';
import './App.css';
import WhenBackendAvailable from './WhenBackendAvailable';
import UserInfo from "./UserInfo";
import Toasts from "./Toasts";

declare global {
    interface Window {
        OIDC_AUTHORITY: string | undefined,
        OIDC_CLIENT_ID: string | undefined,
        OIDC_REDIRECT_URI: string | undefined,
        VERSION: string | undefined,
    }
}

if (window.VERSION) {
    console.log("Version: ", window.VERSION);
}

const App = () => {
    return (
        <div className="main">
            <header>
                IAI-MQTT-UI
            </header>
            <WhenBackendAvailable>
                <LoginManager>
                    <>
                        <UserInfo/>
                        <Projects/>
                    </>
                </LoginManager>
            </WhenBackendAvailable>
            <Toasts/>
        </div>
    );
};

export default App;

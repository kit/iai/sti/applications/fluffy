import {
    useBasicAuthDelete,
    useBasicAuthPatch,
    useBasicAuthPost
} from './auth.hooks';
import {join} from "../utils/utils";
import {ENDPOINT_PROJECTS, useProjectsList, useProjectDetails} from "./projects.hooks";
import {onGeneralError} from "../state/utils";


export const useChangeProjectUsers = (projectId: number) => {
    const projectsList = useProjectsList();
    const projectDetails = useProjectDetails(projectId);

    const reload = () =>
        Promise.all([
            projectsList.refetch(),
            projectDetails.refetch(),
        ]);

    const { mutateAsync: addUser, isPending: isAddingUser } = useBasicAuthPost<{username: string}>(
        join(ENDPOINT_PROJECTS, projectId, 'add-user'), {
            onSuccess: reload,
            onError: (error, payload) => {
                try {
                    let message;
                    if (typeof (error.response.data?.detail) === "string") {
                        message = error.response.data?.detail;
                    } else if (typeof (error.response.data) === "string") {
                        message = error.response.data;
                    } else {
                        message = JSON.stringify(error.response.data);
                    }
                    alert(`${error.response.status} ${error.response.statusText}: ${message}`);
                } catch {
                    console.warn(error, payload);
                    alert(error.message);
                }
            },
        }
    );

    const {
        mutateAsync: changeUserRole,
        isPending: isChangingUserRole
    } = useBasicAuthPatch<number>(
        (userId: number) => join(ENDPOINT_PROJECTS, projectId, 'user', userId, 'toggle-manager'), {
            onSuccess: reload,
            onError: onGeneralError,
        }
    );

    const {
        mutateAsync: removeUser,
        isPending: isRemovingUser
    } = useBasicAuthDelete<number>(
        (userId: number) => join(ENDPOINT_PROJECTS, projectId, 'user', userId), {
            onSuccess: reload,
            onError: onGeneralError,
        }
    );

    const isMutating = isAddingUser || isChangingUserRole || isRemovingUser;

    return {
        addUser,
        isAddingUser,
        changeUserRole,
        isChangingUserRole,
        removeUser,
        isRemovingUser,
        isMutating: isAddingUser || isChangingUserRole || isRemovingUser,
        isPending: isMutating || projectDetails.isPending || projectsList.isPending,
    };
};

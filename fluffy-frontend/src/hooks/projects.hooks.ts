import {QueryOptions, useBasicAuthDelete, useBasicAuthGet, useBasicAuthPost} from './auth.hooks';
import {ProjectOverview, DetailedProject} from "../model";
import {join} from "../utils/utils";
import {onGeneralError} from "../state/utils";
import {queryClient} from "../queryClient";
import React from "react";
import {LdapUser} from "../state/context";
import toast from "react-hot-toast";

export const ENDPOINT_PROJECTS = '/api/projects/projects/';
export const ENDPOINT_LDAP_USERS = ENDPOINT_PROJECTS + 'ldap-users';

export const useProjectsList = (options?: QueryOptions) =>
    useBasicAuthGet<ProjectOverview[]>(['projects'], ENDPOINT_PROJECTS, options);

export const useProjectDetails = (projectId: number, enabled?: boolean) =>
    useBasicAuthGet<DetailedProject>(
        ['project', projectId],
        join(ENDPOINT_PROJECTS, projectId),
        {enabled}
    );

export const useDeleteProject = (projectId: number) => {
    const { refetch: refetchAllProjects } = useProjectsList();

    const query = useBasicAuthDelete(() =>
        join(ENDPOINT_PROJECTS, projectId), {
        onSuccess: refetchAllProjects,
        onError: onGeneralError,
    });

    return {
        deleteProject: query.mutate,
        isDeleting: query.isPending,
    };
};

export const usePostProject = (onSuccess: (data: any) => void) =>
    useBasicAuthPost<any>(ENDPOINT_PROJECTS, {
        onSuccess: (response: any) => {
            queryClient.invalidateQueries({queryKey: ['projects']});
            onSuccess(response.data);
        },
        onError: onGeneralError
    });

// the ldap error number is contained as the "result" field of the corresponding error information.
const LDAP_ERROR = Object.freeze({
    SERVER_DOWN: -1,
    OPERATIONS_ERROR: 1,
    SIZELIMIT_EXCEEDED: 4,
    INVALID_CREDENTIALS: 49
});

const LDAP_ERROR_MESSAGE = Object.freeze({
    [LDAP_ERROR.SERVER_DOWN]:
        "Server down or unreachable. This could also be a problem with your VPN config.",
    [LDAP_ERROR.OPERATIONS_ERROR]:
        "Operations error. Maybe the binding username and password are not given?",
    [LDAP_ERROR.SIZELIMIT_EXCEEDED]:
        "Size limit exceeded - please inform your administrator, that LDAP search base has to be more constrained.",
    [LDAP_ERROR.INVALID_CREDENTIALS]:
        "Invalid credentials. check the binding username and password, if they appear right, consult your administrator."
});

const getErrorMessage = (error: any): string => {
    const info = error.response?.data as any;
    if (typeof(info) === 'string') {
        return info;
    }
    if (!info) {
        return `${error.response?.status} ${error.response?.statusText}`;
    }
    return (
        LDAP_ERROR_MESSAGE[info.result as keyof typeof LDAP_ERROR_MESSAGE]
        ?? info.desc
    );
};

type LdapUserQuery = {
    users: LdapUser[],
    isPending: boolean,
    error: string,
};

export const useGetLdapUsers = (options?: QueryOptions): LdapUserQuery => {
    const {data, isError, error, isPending} = useBasicAuthGet<any[]>(
        ['ldap-users'],
        ENDPOINT_LDAP_USERS,
        options,
    );
    const [errorMessage, setErrorMessage] = React.useState<string>("");

    React.useEffect(() => {
        if (!isError) {
            setErrorMessage("");
        } else if (error.response?.status === 403) {
            setErrorMessage("Unauthorized")
        } else {
            const message = getErrorMessage(error);
            setErrorMessage(message);
            toast.error(`Error reading LDAP Users.\n\n${message}`)
        }
    }, [isError, error])

    return React.useMemo(() => ({
            users: (data?.data ?? []) as LdapUser[],
            error: errorMessage,
            isPending: isPending,
        }),
        [data, errorMessage, isPending]
    );
};
import React from "react";
import {
    useBasicAuthDelete,
    useBasicAuthHeader,
    useBasicAuthPost,
    useBasicAuthPatch
} from "./auth.hooks";
import {join} from "../utils/utils";
import {DetailedProject, Device, NewDevice, Role, RolePermissions} from "../model";
import {useMutation} from "@tanstack/react-query";
import axios from "axios";
import {onGeneralError} from "../state/utils";
import toast from "react-hot-toast";
import {useProjectDetails} from "./projects.hooks";

const ENDPOINT_MQTT_PREFIX = '/api/mqtt/';
const ENDPOINT_MQTT_ROLES = join(ENDPOINT_MQTT_PREFIX, 'roles/');
const ENDPOINT_MQTT_DEVICES = join(ENDPOINT_MQTT_PREFIX, 'devices/');
const ENDPOINT_MQTT_PERMISSIONS = join(ENDPOINT_MQTT_PREFIX, 'permissions/');

export const useRoleMutations = (projectId: number) => {
    const { refetch: refetchProject } = useProjectDetails(projectId);

    const {mutateAsync: mutatePost, isPending: isPosting} =
        useBasicAuthPost<Partial<Role>>(
            ENDPOINT_MQTT_ROLES, {
                onSuccess: refetchProject,
                onError: onGeneralError,
            });

    const postRole = React.useCallback(async (name: string, description: string) =>
            mutatePost({name, description, project: projectId})
    , [mutatePost, projectId]);

    const {mutateAsync: deleteRole, isPending: isDeleting} =
        useBasicAuthDelete((roleId: number) =>
            join(ENDPOINT_MQTT_ROLES, roleId),
            {
                onSuccess: (a: any) => {
                    refetchProject();
                    toast.success('Role successfully deleted')
                },
                onError: onGeneralError
            });

    return {
        refetchProject,
        postRole,
        isPosting,
        deleteRole,
        isDeleting
    };
};

export const usePermissionMutations = (projectId: number) => {
    const { refetch: refetchProject, isPending } = useProjectDetails(projectId);

    const {mutateAsync: postPermissions, isPending: isPosting} =
        useBasicAuthPost<Partial<RolePermissions>>(
            ENDPOINT_MQTT_PERMISSIONS,
            {
                onSuccess: refetchProject,
                onError: onGeneralError,
            });

    const {mutateAsync: patchPermissions, isPending: isPatching} =
        useBasicAuthPatch(
        (p: RolePermissions) =>
            join(ENDPOINT_MQTT_PERMISSIONS, p.id),
            {
                onSuccess: refetchProject,
                onError: onGeneralError,
            });

    const {mutateAsync: deletePermissions, isPending: isDeleting} =
        useBasicAuthDelete((permissionsId: number) =>
            join(ENDPOINT_MQTT_PERMISSIONS, permissionsId),
            {
                onSuccess: refetchProject,
                onError: onGeneralError
            });

    return {
        postPermissions,
        isPosting,
        deletePermissions,
        isDeleting,
        patchPermissions,
        isPatching,
        isPending: isPending || isPosting || isDeleting || isPatching,
    };
};

export const useDeviceMutations = (project: DetailedProject) => {
    const { refetch: refetchProject } = useProjectDetails(project.id);

    const patchMutations = useDevicePatch(project);

    const {mutateAsync, isPending: isPosting} =
        useBasicAuthPost<NewDevice>(ENDPOINT_MQTT_DEVICES, {
            onSuccess: refetchProject,
            onError: onGeneralError,
        });

    const postDevice = React.useCallback((name: string, roleId: number) =>
        mutateAsync({
            name,
            roles: [roleId],
            project: project.id,
        })
    , [mutateAsync, project.id])

    const {mutateAsync: deleteDevice, isPending: isDeleting} =
        useBasicAuthDelete((deviceId: number) =>
            join(ENDPOINT_MQTT_DEVICES, deviceId), {
            onSuccess: refetchProject,
            onError: onGeneralError,
        });

    return {
        postDevice,
        isPosting,
        deleteDevice,
        isDeleting,
        ...patchMutations,
        isPending: isPosting || isDeleting || patchMutations.isPatching,
    };
};

const useDevicePatch = (project: DetailedProject) => {
    const config = useBasicAuthHeader();
    const { refetch: refetchProject } = useProjectDetails(project.id);

    const deviceById = (id: number | undefined) =>
        project.devices.find(device => device.id === id);

    const {mutateAsync: patchDevice, isPending} = useMutation<ResponseType, Device, any, any>({
        mutationFn: (arg: Partial<Device>) =>
            axios.patch(join(ENDPOINT_MQTT_DEVICES, arg.id), arg, config),
        onSuccess: () =>
            refetchProject().catch(console.warn)
    });

    const patchDeviceRoles = async (device: Device | undefined, roles: number[]) => {
        if (!device) {
            return;
        }
        return patchDevice({id: device.id, roles, project: project.id});
    }

    const addDeviceToRole = async (deviceId: number | undefined, roleId: number) => {
        const device = deviceById(deviceId);
        if (!device) {
            return;
        }
        const roles = [...device.roles, roleId];
        return patchDeviceRoles(device, roles);
    };

    const removeDeviceFromRole = async (deviceId: number | undefined, roleId: number) => {
        const device = deviceById(deviceId);
        if (!device) {
            return;
        }
        const roles = device.roles.filter(id => id !== roleId);
        return patchDeviceRoles(device, roles);
    };

    const { mutateAsync: resetDevicePassword } = useMutation<any, number, any, any>({
        mutationFn: (deviceId: number) =>
            axios.patch(join(ENDPOINT_MQTT_DEVICES, deviceId, '/reset-password/'), {}, config),
        onSuccess: console.log,
        onError: onGeneralError,
    });

    return {
        addDeviceToRole,
        removeDeviceFromRole,
        isPatching: isPending,
        resetDevicePassword
    };
};

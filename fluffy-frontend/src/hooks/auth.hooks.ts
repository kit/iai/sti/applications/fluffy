import {
    useMutation,
    UseMutationOptions,
    UseMutationResult,
    useQuery,
    UseQueryOptions,
    UseQueryResult
} from '@tanstack/react-query';
import axios, {AxiosRequestConfig, AxiosResponse} from "axios";
import {useAuth} from "react-oidc-context";
import React from "react";


export type BasicAuthHeader = Partial<AxiosRequestConfig>;

const readCookie = (name: string) =>
    document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')?.pop() || '';

export const useBasicAuthHeader = (params?: object): BasicAuthHeader => {
    const oidcAuth = useAuth();
    const csrfToken = readCookie('csrftoken');

    return React.useMemo(() => {
        const result: any = {
            headers: { 'Content-Type': 'application/json' },
            withCredentials: true,
            params
        };

        if (oidcAuth.isAuthenticated) {
            // OIDC Bearer Authorization
            result.headers['Authorization'] = 'Bearer ' + oidcAuth.user?.access_token;
            result.headers['X-CSRFToken'] = csrfToken;
        }

        return result as BasicAuthHeader;
    }, [params, oidcAuth, csrfToken]);
};

const withQueryParam = (url: string, queryParam: Record<string, any> | undefined): string => {
    const query = (new URLSearchParams(queryParam)).toString();
    if (!query) {
        return url;
    }
    return `${url}?${query}`;
};

export type QueryOptions = Omit<UseQueryOptions, "queryKey">;

export const useBasicAuthGet = <ResponseType>(
    queryKey: (string | number)[],
    url: string,
    options?: QueryOptions,
    queryParam?: Record<string, any>,
): UseQueryResult<AxiosResponse<ResponseType>, any> => {
    const config = useBasicAuthHeader();
    const queryFn = React.useCallback(() =>
            axios.get(withQueryParam(url, queryParam), config)
        , [url, queryParam, config]
    );
    return useQuery<any>({
        queryKey,
        queryFn,
        ...options
    });
};

export const useBasicAuthPost = <ArgType, ResponseType = any>(
    url: string | ((a: ArgType) => string),
    options?: UseMutationOptions<ResponseType, any, ArgType>,
): UseMutationResult<ResponseType, any, ArgType, any> => {
    const config = useBasicAuthHeader();
    const mutationFn = (arg: ArgType): Promise<ResponseType> => {
        const _url = typeof(url) === "function"
            ? url(arg)
            : url;
        const _arg = typeof(url) === "function"
            ? {}
            : arg;
        return axios.post(_url, _arg, config);
    };
    return useMutation<ResponseType, ArgType, any, any>({
        mutationFn,
        ...options
    });
};

export const useBasicAuthDelete = <ArgType, ResponseType = any>(
    url: (a: ArgType) => string,
    options?: UseMutationOptions<ResponseType, any>,
) => {
    const config = useBasicAuthHeader();
    const mutationFn = (arg: ArgType): Promise<ResponseType> =>
        axios.delete(url(arg), config);
    return useMutation<ResponseType, any, any, any>({
        mutationFn,
        ...options
    });
};

export const useBasicAuthPatch = <ArgType, ResponseType = any>(
    url: (a: ArgType) => string,
    options?: UseMutationOptions<ResponseType, any, ArgType>,
): UseMutationResult<ResponseType, any, ArgType, any> => {
    const config = useBasicAuthHeader();
    const mutationFn = (arg: ArgType): Promise<ResponseType> =>
        axios.patch(url(arg), arg, config);
    return useMutation<ResponseType, ArgType, any, any>({
        mutationFn,
        ...options
    });
};

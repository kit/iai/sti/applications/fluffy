import React from 'react';

type InputEvent = React.SyntheticEvent<HTMLInputElement>;

export function useInputState(initialState: string = ""): [string, (event: InputEvent) => void, () => void] {
    const [state, setState] = React.useState<string>(initialState);
    const setStateHandler = (event: InputEvent) => setState((event.target as HTMLInputElement).value);
    const resetState = () => setState(initialState);
    return [state, setStateHandler, resetState];
}

export function useSessionStoredState<T>(initialState: T, key: string)
    : [T,  React.Dispatch<React.SetStateAction<T>>] {

    const fullKey = React.useMemo(() => `iai.mqtt.${key}`, [key]);

    const [value, setValue] = React.useState<T>(() => {
        const stored = sessionStorage.getItem(fullKey);
        try {
            return JSON.parse(stored ?? "");
        } catch(e) {
            return initialState;
        }
    });

    React.useEffect(() => {
        sessionStorage.setItem(fullKey, JSON.stringify(value));
    }, [fullKey, value]);

    return [value, setValue];
}

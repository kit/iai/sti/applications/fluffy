import React from "react";
import axios from "axios";
import { SevereWarning } from "./utils/Components";

const SKIP_AVAILABILITY_REQUEST = false;

const WhenBackendAvailable = ({children}: {children: React.ReactElement}) => {
    const [delayWarning, setDelayWarning] = React.useState(2000);
    const [apiAvailable, setApiAvailable] = React.useState(false);
    const checkedAlready = React.useRef<boolean>(false);

    React.useEffect(() => {
        let mounted = true;
        if (SKIP_AVAILABILITY_REQUEST) {
            return;
        }
        if (checkedAlready.current) {
            return;
        }
        axios.get("/is-responding")
            .then(() => {
                setApiAvailable(true);
            })
            .catch(error => {
                if (!mounted) {
                    return;
                }
                setApiAvailable(error.response?.status !== 500);
            })
            .finally(() => {
                checkedAlready.current = true;
            });
        return () => {
            mounted = false
        };
    }, []);

    React.useEffect(() => {
        let timeout: NodeJS.Timeout;
        if (delayWarning > 0) {
            timeout = setTimeout(() => {
                setDelayWarning(0);
            }, delayWarning);
        }
        return () =>
            clearTimeout(timeout);
    }, [delayWarning]);

    if (SKIP_AVAILABILITY_REQUEST) {
        return children;
    }

    return apiAvailable
        ? children
        : delayWarning > 0
            ? null
            : <SevereWarning>
                Server application does not respond. Is it running?
            </SevereWarning>;
};

export default WhenBackendAvailable;
import React from "react";
import {useAuth} from "react-oidc-context";
import styled from "@emotion/styled";

export const useOicdUserName = () => {
    const oicdAuth = useAuth();
    return oicdAuth.user?.profile.preferred_username ?? "(unknown)";
};

const UserInfo = () => {
    const oicdAuth = useAuth();
    const username = useOicdUserName();

    return (
        <UserInfoPanel>
            <div style={{textAlign: 'right'}}>
                Logged in as: {username}
            </div>
            <button onClick={() => oicdAuth.removeUser()}>
                Logout
            </button>
        </UserInfoPanel>
    );
};

export default UserInfo;

const UserInfoPanel = styled.div`
    max-width: 360px;
    display: flex;
    flex-direction: column;
    align-self: flex-end;
    font-size: smaller;
`;

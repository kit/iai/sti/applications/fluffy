import React from "react";
import {
    FlexColumn,
    LeftAlign,
    DashedList,
    WithSideColumn, ThreeDotMenu, DashedListItemRow
} from "./utils/Components";
import {ProjectOverview, User} from "./model";
import {useChangeProjectUsers} from "./hooks/users.hooks";
import {MenuItem} from "@szhsin/react-menu";
import {ImUserMinus} from "react-icons/im";
import {FaUserAlt} from "react-icons/fa";
import {ProjectAddMembers} from "./ProjectAddMembers";
import {useOicdUserName} from "./UserInfo";
import {LoadingSpinnerOverlay} from "./utils/LoadingSpinner";

export const ProjectMembers = ({project, visible}: { project: ProjectOverview, visible: boolean }) => {
    if (!visible) {
        return null;
    }
    return (
        <WithSideColumn>
            <ExistingProjectMembers project={project}/>
            <ProjectAddMembers project={project}/>
        </WithSideColumn>
    );
};

export default ProjectMembers;


const ExistingProjectMembers = ({project}: { project: ProjectOverview }) => {
    const {changeUserRole, removeUser, isPending} = useChangeProjectUsers(project.id);
    const loggedInUsername = useOicdUserName();

    const userLists = [{
        list: project.managers,
        isManager: true
    }, {
        list: project.members,
        isManager: false
    }];

    const isLoggedInAsNonSuperUser = (user: User) =>
        user.username === loggedInUsername && !project.isManager;

    const onChangeManagerStatus = (user: User, isManager: boolean) => {
        changeUserRole(user.id)
            .catch(console.warn);
    };

    const onRemoveUser = (user: User) => {
        removeUser(user.id)
            .catch(console.warn);
    };

    return (
        <FlexColumn>
            <LoadingSpinnerOverlay
                active={isPending}
            />
            <LeftAlign>
                Users
            </LeftAlign>
            <DashedList>
                {
                    project.members.length === 0 && project.managers.length === 0 &&
                    <span style={{opacity: 0.3}}>
                        This project has no users.
                    </span>
                }
                {
                    userLists.map(
                        ({list, isManager}: { list: User[], isManager: boolean }) =>
                            list.map(user =>
                                <UserEntry
                                    user={user}
                                    canEdit={project.canEditMembers}
                                    isManager={isManager}
                                    key={user.id}
                                >
                                    <MenuItem
                                        onClick={() => onChangeManagerStatus(user, isManager)}
                                        disabled={isLoggedInAsNonSuperUser(user) && isManager}
                                    >
                                        <FaUserAlt style={{marginRight: '0.6rem'}}/>
                                        {
                                            isManager
                                                ? isLoggedInAsNonSuperUser(user)
                                                    ? "This is you - Ask a Superuser or other Manager to remove your Manager status."
                                                    : 'Remove Manager Status'
                                                : 'Give Manager Status'
                                        }
                                    </MenuItem>
                                    <MenuItem
                                        onClick={() => onRemoveUser(user)}
                                        disabled={isLoggedInAsNonSuperUser(user) && isManager}
                                    >
                                        <ImUserMinus style={{marginRight: '0.6rem'}}/>
                                        {
                                            isManager && isLoggedInAsNonSuperUser(user)
                                                ? 'This is you - Ask a Superuser or other Manager to remove you from this project.'
                                                : 'Remove User from Project'
                                        }

                                    </MenuItem>
                                </UserEntry>
                            )
                    )
                }
            </DashedList>
        </FlexColumn>
    );
};

const nameFor = (user: User) => {
    if (!user.last_name) {
        return user.username;
    }
    if (!user.first_name) {
        return user.last_name;
    }
    return `${user.last_name}, ${user.first_name}`;
};

const UserEntry = ({user, canEdit, isManager, children, buttonTooltip = ""}: {
    user: User,
    canEdit?: boolean,
    isManager?: boolean,
    buttonTooltip?: string
    children: any
}) => {
    const [hover, setHover] = React.useState(false);
    return (
        <DashedListItemRow
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
        >
            {
                canEdit &&
                <ThreeDotMenu
                    tooltip={buttonTooltip}
                    onlyHint={!hover}
                    children={children}
                />
            }
            <span style={{fontSize: 'smaller'}}>
            {
                isManager
                    ? <span style={{color: '#ff6a'}}>
                        Manager
                    </span>
                    : <span style={{color: '#aaaa'}}>
                        Member
                    </span>
            }
            </span>
            <div style={{flex: 1, textAlign: "left", paddingLeft: "0.25rem"}}>
                {nameFor(user)}
            </div>
        </DashedListItemRow>
    )
};

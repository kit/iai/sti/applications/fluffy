import React, {useState} from 'react';
import {useProjectsList, usePostProject} from "./hooks/projects.hooks";
import {PlusButton, FlexRow, MainBlock, Warning} from './utils/Components';
import LoadingSpinner, {LoadingSpinnerOverlay} from "./utils/LoadingSpinner";
import InlineForm from './utils/InlineForm';
import {whenEnter} from './utils/formUtils';
import {useSearchParams} from "react-router-dom";
import {ProjectOverview} from "./model";
import {OrderingInfo, ProjectBlock, ProjectsList,} from "./ProjectsStyles";
import {useSessionStoredState} from "./hooks/hooks";
import {nextIndex} from "./utils/utils";
import {inOrder, Order} from "./utils/ordering";
import toast from "react-hot-toast";
import {ProjectItem} from "./ProjectItem";


const Projects = () => {
    return (
        <div>
            <AddNewProject/>
            <ListProjects/>
        </div>
    );
};

export default Projects;


const ListProjects = () => {
    const query = useProjectsList();
    const [projects, setProjects] = useState<ProjectOverview[]>([]);
    const [order, setOrder] = useSessionStoredState<Order>(Order.Alphabetical, "project.order");

    React.useEffect(() => {
        if (!query.data?.data) {
            return;
        }
        setProjects(inOrder(query.data!.data, order));
    }, [query.data, order]);

    if (query.isLoading) {
        return <LoadingSpinner title={"Loading projects"}/>;
    }

    if (query.isError) {
        const message = query.error.response
            ? `Error in GET projects (${query.error.response.status}: ${query.error.response.data.detail})`
            : 'Undefined Error in GET projects.';
        return (
            <Warning>
                {message}
            </Warning>
        );
    }

    return (
        <ProjectBlock>
            <FlexRow>
                <div style={{flex: 1}}>Projects</div>
                <OrderingOption
                    order={order}
                    onClick={() => setOrder(nextIndex(order, Order))}
                    disabled={projects.length === 0}
                />
            </FlexRow>
            <div style={{fontSize: 'smaller', opacity: 0.7}}>
                Click on a project to expand information / add roles
            </div>
            <ProjectsList>
                <ProjectsListContent projects={projects}/>
            </ProjectsList>
        </ProjectBlock>
    );
};

const ProjectsListContent = ({projects}: {projects: ProjectOverview[]}) => {
    const [params] = useSearchParams();
    const projectParam = params.get('project');
    const [lastOpenedId, setLastOpenedId] = useSessionStoredState<number | null>(null, "project.id");

    if (!projects.length) {
        return (
            <span>
                You do not have any projects assigned yet.
            </span>
        );
    }

    return <>{
        projects.map((project: any) =>
            <ProjectItem
                project = {project}
                key = {project.id}
                opened={
                    projectParam !== null
                        ? project.id === +projectParam
                        : project.id === lastOpenedId
                }
                onOpen = {(open) =>
                    setLastOpenedId(open ? project.id : null)
                }
            />
        )
    }</>;
};

const AddNewProject = () => {
    const [name, setName] = React.useState<string>("");
    const [, setSearchParams] = useSearchParams();
    const {mutate: post, isPending} = usePostProject(
        (data) => {
            toast.success(
                `Project "${data.name}" successfully created."`
            );
            setSearchParams({project: data.id});
        }
    );

    const submit = () => post({name});

    return (
        <MainBlock>
            <LoadingSpinnerOverlay active={isPending}/>
            <InlineForm pending={isPending}>
                <div>Add new project:</div>
                <input
                    value = {name}
                    onChange = {event => setName(event.target.value)}
                    onKeyDown= {whenEnter(submit)}
                    placeholder = "project name"
                    disabled = {isPending}
                    style = {{flex: 1}}
                />
                <PlusButton
                    onClick = {submit}
                    disabled = {isPending || !name}
                />
            </InlineForm>
        </MainBlock>
    );
};


const OrderingOption = ({order, onClick, disabled = false}: {
    order: Order,
    onClick: () => void,
    disabled: boolean
}) => {
    const content = (
        order === Order.Alphabetical
        ? "\u2193 abc"
        : order === Order.AntiAlphabetical
        ? "\u2191 abc"
        :"\u21f5"
    );

    const title = (
        order === Order.Alphabetical
        ? "in alphabetical order"
        : order === Order.AntiAlphabetical
        ? "in reverse alphabetical order"
        : "in order of creation (by id)"
    )

    return (
        <OrderingInfo
            onClick={onClick}
            disabled={disabled}
            title={title}
        >
            {content}
        </OrderingInfo>
    );
};

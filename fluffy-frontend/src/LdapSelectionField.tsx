import React from "react";
import {LdapUser, useStore} from "./state/context";
import LoadingSpinner from "./utils/LoadingSpinner";
import Select from "./utils/Select";
import {SelectOption} from "./utils/formUtils";
import {INPUT_WIDTH} from "./ProjectAddMembers";
import {Project} from "./model";

export type SelectedUser = SelectOption<LdapUser> | null;


export const LdapSelectionField = ({user, setUser, project, loading, options}: {
    loading: boolean
    options: SelectOption<LdapUser>[] | null,
    user: SelectedUser,
    setUser: React.Dispatch<React.SetStateAction<SelectedUser>>,
    project: Project
}) => {
    const {ldapPending} = useStore();

    const alreadyUsedKitIds = React.useMemo(() => {
        const result: string[] = [];
        for (const userList of [project.managers, project.members]) {
            for (const user of userList) {
                result.push(user.username);
            }
        }
        return result;
    }, [project]);

    const remainingUsers = React.useMemo(() =>
            (options ?? [])
                .filter(user =>
                    !alreadyUsedKitIds.includes(user.value.sAMAccountName)
                )
        , [options, alreadyUsedKitIds]);

    const noOptions = !options?.length;
    const noUsers = options !== null && noOptions;

    if (loading) {
        return (
            <div style={{fontSize: 'small', display: "flex", alignItems: "center"}}>
                <LoadingSpinner size={20}/>
                <span>Retrieving IAI users from LDAP...</span>
            </div>
        );
    }

    return (
        <div>
            <div style={{marginBottom: "0.25rem"}}>
                LDAP user:
            </div>
            <Select
                options = {remainingUsers}
                onChange = {option => setUser(option as SelectedUser)}
                value = {user}
                placeholder = {
                    ldapPending
                        ? "Loading IAI users..."
                        : noUsers
                            ? "No LDAP users were found."
                            : "Select IAI user..."}
                isDisabled = {noOptions}
                style = {{width: INPUT_WIDTH}}
            />
        </div>
    );
};
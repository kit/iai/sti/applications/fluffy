import styled from "@emotion/styled";
import {SectionBlock} from "./utils/Components";

export const ProjectBlock = styled(SectionBlock)`
  border-color: black;
  background-color: #fff1;
`;

export const ProjectsList = styled.ul`
  display: flex;
  flex-direction: column;
  list-style: none;
  margin: 0;
  padding: 0.5rem 0;
  gap: 0.5rem;
   
  & > span {
    opacity: 0.5;
  }
`;

export const PROJECT_ACTIVE_SHADE = '#0833ff18';

export const ProjectsListItem = styled.li<{ highlight?: boolean }>`
  display: flex;
  align-items: baseline;
  padding: 0.25rem 1rem;
  cursor: pointer;
  background-color: ${props => props.highlight ? PROJECT_ACTIVE_SHADE : 'unset'};
  color: ${props => props.highlight ? '#ffbd' : 'unset'};
  font-weight: ${props => props.highlight ? 'bold' : 'unset'};
  max-width: 100%;
      
  &:hover {
    background-color: ${props => props.highlight ? '' : PROJECT_ACTIVE_SHADE};
  }
    
  & > div {
    &:nth-of-type(1) {
      flex: 1 1 100%;
      text-align: left;
      display: flex;
      align-items: baseline;
      gap: 1rem;
      font-size: 1.1em;
    }
    
    &:nth-of-type(2) {
      flex: 1 1 40%;
      opacity: 0.5;
    }
  }
`;

export const ProjectEntryFrame = styled.div`
  border: 2px solid transparent;
  background-color: #0002;
  border-radius: 2px;
  box-shadow: 1px 1px 2px #0003 inset;
`;

export const ProjectDetailsEntryFrame = styled.div`
  background-color: ${PROJECT_ACTIVE_SHADE};
  padding: 0.25rem;
  min-height: 3rem;
`;

export const Badge = styled.div`
  padding: 0.3rem 0.7rem;
  width: fit-content;
  font-size: small;
  font-weight: normal;
  opacity: 0.7;
  background-color: #ffffff14;
  border-radius: 20px;
  box-shadow: 2px 2px 3px -1px #fff4 inset;
`;

export const OrderingInfo = styled.div<{ disabled?: boolean }>`
    opacity: 0.6;
    color: white;
    cursor: pointer;
    font-size: small;
    
    ${(props) => props.disabled ? `
        visibility: collapsed;
    ` : ''}

    &:hover {
        opacity: 0.8;
    }
`;
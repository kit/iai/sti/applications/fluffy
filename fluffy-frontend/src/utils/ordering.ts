export enum Order {
    AsIs,
    Alphabetical,
    AntiAlphabetical
}

export const inOrder = <T extends { name: string }>(data: T[], order: Order = Order.Alphabetical): T[] => {
    if (order === Order.AsIs) {
        return data;
    }
    const sign = order === Order.AntiAlphabetical ? -1 : 1;
    return data.toSorted(
        (a, b) => a.name.toLowerCase() < b.name.toLowerCase() ? -sign : +sign
    );
};

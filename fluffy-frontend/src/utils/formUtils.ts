import React from 'react';

export type SelectOption<T> = { value: T, label: string };

type KeyInputEvent = React.KeyboardEvent<HTMLInputElement>;

export const whenEnter = (onEnter: (event: KeyInputEvent) => void) => (event: KeyInputEvent) => {
    if (event.key === 'Enter') {
        onEnter(event);
    }
};

export const whenEnterIf = (enabled = true, action: (...args: any[]) => void) =>
    (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter" && enabled) {
            action();
        }
    };
import { UseQueryResult } from "@tanstack/react-query";

export const hasErrorCode = (query: UseQueryResult<any, any>, code: number) : boolean =>
    query.isError && query.error.response.status === code;

export const isUnauthorized = (query: UseQueryResult) : boolean =>
    hasErrorCode(query, 401);

export const isForbidden = (query: UseQueryResult) : boolean =>
    hasErrorCode(query, 403);

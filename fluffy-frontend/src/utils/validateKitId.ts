
const KIT_STUDENT_REGEX = /^[a-z]{5}$/;

const KIT_EMPLOYEE_REGEX = /^[a-z]{2}\d{4}$/;

export const isValidKitId = (id: string) => {
    return KIT_EMPLOYEE_REGEX.test(id) || KIT_STUDENT_REGEX.test(id);
};

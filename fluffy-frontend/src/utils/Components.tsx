import styled from "@emotion/styled";
import {INPUT_WIDTH} from "../ProjectAddMembers";
import {BsThreeDots} from "react-icons/bs";
import React from "react";
import {Menu} from "@szhsin/react-menu";

export const MainBlock = styled.div`
  padding: 0.5rem 1rem;
  margin: 1rem;
  text-align: left;
  max-width: 100%;
  min-width: 60%;
  border: 2px solid black;
  border-radius: 4px;
  box-shadow: 2px 2px 4px #0003;
`;

export const AddBlock = styled(MainBlock)`
  margin: 1rem 0;
  padding: 0.5rem;
`;

export const Warning = styled(MainBlock)`
  border: 2px solid red;
  background-color: #400;
  color: red;
  font-size: larger;
  font-weight: bold;
  text-align: center;
`;

export const SevereWarning = styled(Warning)`
  background-color: #100;
  color: #b00;
  border: 2px solid red;
`;

export const FlexColumn = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  gap: 0.5rem;
`;

export const FlexRow = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: baseline;
  gap: 0.5rem;   
`;

export const WithBorderSpacing = styled.div`
  border: 1px solid #fff1;
  margin: 0.2rem 0;
  padding: 0.5rem;
  width: unset;
  box-shadow: 1px 1px 3px #0003 inset; 
  border-radius: 4px;
`;

export const WithSideColumn = styled(WithBorderSpacing)`
  display: grid;
  grid-template-columns: 1fr auto;
  grid-gap: 1rem;
`;

export const SideColumn = styled.div`
    text-align: left;
    padding: 0.5rem;
    display: flex;
    flex-direction: column;
    gap: 0.25rem;
    align-items: stretch;
    font-size: smaller;
    
    & > .error {
        font-size: larger;
        font-weight: bold;
        color: #f31;
    }
    
    & button {
        margin: 0.25rem 0;
    }

    & input {
        width: ${INPUT_WIDTH};
        margin: 0;
        box-sizing: border-box;
    }
    
    & > div:first-of-type {
        font-size: larger;
    }
`;

export const SectionBlock = styled(MainBlock)`
  & > div:first-of-type {
    font-size: larger;
    padding-bottom: 0.5rem;
  }
`;

export const SquareButton = styled.button`
  padding: 0.1rem 0.4rem 0 0.4rem;
  margin: 0.1rem;
  width: 2rem;
  height: 2rem;
  font-size: 1.2rem;
`;

export const PlusButton = styled(SquareButton)`
  padding: 0 0.5rem 0 0.5rem;
  font-size: 1.5rem;
  transform: translateY(2px);
    
  &::after {
    content: '+';
  }
`;

export const LinkSpan = styled.span<{hidden?: boolean}>`
  display: ${props => props.hidden ? 'none' : 'unset'};
  cursor: pointer;
  text-decoration: underline;
`;

export const SmallHint = styled.div`
    opacity: 0.3;
    font-size: smaller;
`;

export const DashedList = styled.ul`
  //display: flex;
  //flex-flow: row wrap;
  //align-items: center;
  gap: 1rem;
  list-style: none;
  position: relative;
        
  & > div {
    display: flex;
    align-items: center;
    padding: 0.25rem 0.5rem;
    border-radius: 3px;
    background-color: #fff2;
    
    & > button {
      margin: 0 0 0 0.5rem;
      padding: 0.2rem 0.4rem 0.1rem;
    }
  }
`;

export const DashedListItem = styled.li`
    transition: 200ms background-color;
    
    &:hover {
        background-color: #fff1;
        transition: 0ms background-color;
    }

    &::before {
        content: '\u2014';
        position: absolute;
        left: 0.7rem;
    }
`;

export const DashedListItemRow = styled(DashedListItem)`
    display: flex;
    flex-flow: row wrap;
    align-items: center;
    gap: 0.5rem;
    justify-content: space-between;
    width: 100%;
    padding-left: 0.25rem;
    border-bottom: 1px solid #fff1;

    &:first-of-type {
        border-top: 1px solid #fff1;
    }
    
    & button {
        padding: 0.25rem;
        margin: 0.25rem;
    }
`;


export const LinkStyle = styled.div`
  text-decoration: underline;
  cursor: pointer;
  color: lightblue;
  
  &:hover {
    color: white;
  }
`;

export const LeftAlign = styled.span`
  text-align: left;
`;

export const ThreeDotMenu = ({tooltip, onlyHint, children}: {tooltip?: string, onlyHint?: boolean, children: React.ReactNode}) =>
    <Menu
        menuButton={
            <button
                title={tooltip}
                style={{
                    background: onlyHint ? 'none' : undefined,
                    height: onlyHint ? "0px" : "1.7rem",
                    color: onlyHint ? '#bbb8' : undefined
                }}>
                <BsThreeDots
                    style={{
                        transform: onlyHint ? "translate(0, -8px)" : "translate(0, 2px)"
                    }}
                />
            </button>
        }
        children={children}
    />;

import React from "react";
import { TailSpin } from "react-loader-spinner";

const LoadingSpinner = ({title, active=true, size=40, compact, style}: {
    title?: string,
    active?: boolean,
    size?: number,
    compact?: boolean,
    style?: React.CSSProperties,
}) =>
    !active ? null :
    <div style={{
        margin: compact ? 0 : 10,
        padding: compact ? 0 : 5,
        ...style
    }}>
        <TailSpin
            color="#00BFFF"
            height={size}
            width={size}
            wrapperClass={'spinner-flex'}
            wrapperStyle={!title ? {marginBottom: "0"} : {}}
        />
        <div>{title}</div>
    </div>;

export default LoadingSpinner;


export const LoadingSpinnerOverlay = ({active=true, style}: {active?: boolean, style?: React.CSSProperties}) =>
    <div style={{
        position: "absolute",
        left: 0,
        right: 0,
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        marginTop: "-0.5rem",
        ...style,
    }}>
        <LoadingSpinner active={active}/>
    </div>;

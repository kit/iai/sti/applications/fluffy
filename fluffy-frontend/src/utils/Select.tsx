import React, {CSSProperties} from "react";
import ReactSelect, {Props} from "react-select";

const Select = ({style, ...props}: Props & { style?: CSSProperties}) =>
    <ReactSelect
        menuPlacement={"auto"}
        styles ={{
            option: (provided) => ({
                ...provided,
                color: 'black',
                textAlign: 'left',
                fontSize: 'smaller'
            }),
            control: (provided) => ({
                ...provided,
                borderRadius: 1,
                opacity: props.isDisabled ? 0.7 : 0.9,
                minWidth: 250,
                textAlign: 'left',
                minHeight: 'unset',
                ...style,
            }),
            dropdownIndicator: (provided) => ({
                ...provided,
                padding: "0.25rem"
            }),
        }}
        {...props}
    />;

export default Select;

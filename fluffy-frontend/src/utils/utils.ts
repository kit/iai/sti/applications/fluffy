export const join = (...args: any[]) => {
    let result = '';
    for(let a = 0; a < args.length; a++) {
        result += args[a].toString();

        // Django backend has APPEND_SLASH standard, so we always include one
        result += '/';
    }
    return result.replaceAll(/\/\//g, '/');
};

type EnumObj = { [K: number | string]: number | string };

export const nextIndex = <E extends EnumObj>(value: number | string, enumObj: E) => {
    const index = typeof value === "number" ? value : enumObj[value] as number;
    return enumObj[(enumObj[index + 1] ?? enumObj[0])] as number;
};
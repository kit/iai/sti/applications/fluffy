import styled from "@emotion/styled";

const InlineForm = styled.div<{pending?: boolean}>`
  display: flex;
  flex-flow: row wrap;
  align-items: center;
  justify-content: stretch;
  padding: 0.5rem 0.25rem;
  margin-right: 0.85rem;
  border-radius: 4px;
  gap: 0.5rem;
    
  ${props => props.pending ? `
      opacity: 0.5;
      transition: opacity 300ms;
    ` : ''}
    
  & > div:first-of-type {
    min-width: 6%;
    font-size: smaller;
    text-align :left;
  }
  
  & input {
    flex: 1;
    margin: 0;
  }
  
  & button {
    border-radius: 0;
    font-weight: bold;
    margin: 0;
    transform: translateY(-1px);
  }
`;

export default InlineForm;

import React from "react";
import {useGetLdapUsers} from "../hooks/projects.hooks";
import {useAuth} from "react-oidc-context";
import {SelectOption} from "../utils/formUtils";

export type LdapUser = {
    cn: string,
    displayName: string,
    sAMAccountName: string,
    userPrincipalName: string,
    givenName: string,
    sn: string,
};

export type Context = {
    userOptions: SelectOption<LdapUser>[] | null,
    ldapPending: boolean,
    errors: Record<string, string>,
};

export const defaultState: Context = {
    userOptions: null,
    ldapPending: false,
    errors: {
        ldap: ""
    },
};

const Store = React.createContext<Context>(defaultState);

export const useStore = () => React.useContext(Store);

const userOptionsFrom = (data: LdapUser[]) =>
    data.map(entry => ({
        label: `${entry.displayName} / ${entry.sAMAccountName}`,
        value: entry,
    })).sort((a, b) =>
        a.label < b.label ? -1 : 1
    );


export const StoreProvider = ({children}: {children: React.ReactElement}) => {
    const [userOptions, setUserOptions] =
        React.useState<SelectOption<LdapUser>[]>([]);
    const {isAuthenticated} = useAuth();

    const ldapQuery = useGetLdapUsers({
        enabled: isAuthenticated,
    });

    React.useEffect(() => {
        if (ldapQuery.error) {
            return;
        }
        setUserOptions(userOptionsFrom(ldapQuery.users));
    }, [ldapQuery]);

    return (
        <Store.Provider
            value = {{
                userOptions,
                ldapPending: ldapQuery.isPending,
                errors: {
                    ldap: ldapQuery.error
                },
            }}
            children={children}
        />
    );
};

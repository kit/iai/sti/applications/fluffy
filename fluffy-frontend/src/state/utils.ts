import {UseQueryResult} from "@tanstack/react-query";
import toast from "react-hot-toast";

const SEPARATOR = ' \n\n';

export const onGeneralError = (err: any) => {
    console.log(err.response);
    let message = `${err.response.status} ${err.response.statusText}`;
    let data = err.response.data;
    if (typeof(data) === "string") {
        // let through backend HTML displays
        if (data.includes('<!DOCTYPE')) {
            document.body.innerHTML = data;
            return;
        }
        message += SEPARATOR + err.response.data;
    } else if (typeof(data) === "object") {
        message += SEPARATOR + Object.values(data).join(SEPARATOR);
    }
    toast.error(message);
};

export const dataOrNull = (query: UseQueryResult<any>) =>
    !query.isFetching && !query.isLoading ? query.data?.data : null;

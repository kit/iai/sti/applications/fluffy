import {ProjectOverview, Project} from "./model";
import React from "react";
import {useChangeProjectUsers} from "./hooks/users.hooks";
import {SideColumn} from "./utils/Components";
import {LdapSelectionField, SelectedUser} from "./LdapSelectionField";
import {useStore} from "./state/context";
import {whenEnter} from "./utils/formUtils";

export const INPUT_WIDTH = "10vw";

const projectContainsUser = (project: Project, username: string) => {
    const findUser = (list: {username: string}[]) => list.find(u => u.username === username);
    return findUser(project.managers) || findUser(project.members);
};

export const ProjectAddMembers = ({project}: { project: ProjectOverview }) => {
    const [selectedUser, setSelectedUser] = React.useState<SelectedUser>(null);
    const [username, setUsername] = React.useState<string | undefined>();
    const {addUser} = useChangeProjectUsers(project.id);
    const mounted = React.useRef(true);
    const {userOptions} = useStore();

    React.useEffect(() => {
        return () => {
            mounted.current = false;
        }
    }, []);

    React.useEffect(() => {
        if (!selectedUser) {
            return;
        }
        setUsername(selectedUser.value.sAMAccountName);
    }, [selectedUser]);

    const onInputKitId = (event: React.ChangeEvent<HTMLInputElement>) => {
        const id = event.target.value.toLowerCase();
        setUsername(id);
        const userLookUp = userOptions?.find(
            user => user.value.sAMAccountName === id
        );
        if (userLookUp) {
            setSelectedUser(userLookUp);
        } else {
            setSelectedUser(null);
        }
    };

    const onSubmit = () => {
        if (!username) {
            return;
        }
        if (projectContainsUser(project, username)) {
            alert(`Project already contains user "${username}".`);
            return;
        }
        addUser({username})
            .then(() => mounted.current && setSelectedUser(null))
            .catch(console.warn);
    };

    if (!project.canEditMembers) {
        return null;
    }

    return (
        <SideColumn>
            <div>
                Add user to project:
            </div>
            <LdapSelectionField
                project={project}
                user={selectedUser}
                setUser={setSelectedUser}
                loading={!userOptions}
                options={userOptions}
            />
            <div>
                KIT username:
            </div>
            <input
                value={username ?? ""}
                onChange={onInputKitId}
                onFocus={e => e.target.select()}
                onKeyDown={whenEnter(onSubmit)}
                placeholder={"ab1234"}
            />
            {
                username === "" &&
                <div className="error">
                    KIT username must be given.
                </div>
            }
            <button
                onClick={onSubmit}
                disabled={!username}
                style={{fontWeight: 'normal', fontSize: "1.15em", width: INPUT_WIDTH}}
                type="button"
            >
                + Add User as Member
            </button>
            <div style={{fontSize: "0.95em", opacity: 0.7, textAlign: "right", maxWidth: INPUT_WIDTH}}>
                Members can be made Managers afterwards.
            </div>
        </SideColumn>
    );
};

import React from "react";
import { AuthProvider } from 'react-oidc-context';

const _window = window as any;
const oidcConfig = {
    authority: _window.OIDC_AUTHORITY || "https://oidc.scc.kit.edu/auth/realms/kit",
    client_id: _window.OIDC_CLIENT_ID || "iai-mqtt-ui-iai-kit-edu",
    redirect_uri: _window.OIDC_REDIRECT_URI || window.origin,
};

if (window.location.search.includes("debug")) {
    console.log("OIDC config", oidcConfig);
}

const OidcProvider = ({children}: {children: React.ReactElement}) => {

    const onSignInCallback = () => {
        window.history.replaceState({}, document.title, window.location.pathname);
    };

    return (
        <AuthProvider {...oidcConfig} onSigninCallback={onSignInCallback}>
            {children}
        </AuthProvider>
    );
};

export default OidcProvider;
export type Project = {
    id: number,
    name: string,
    roottopic: string,
    managers: User[],
    members: User[],
};

export type HasProjectId = { projectId: number };

export type UserRoleInfo = {
    isManager: boolean,
    isMember: boolean,
    isSuperUser: boolean,
    canEditMembers: boolean,
}

export type ProjectOverview = Project & UserRoleInfo & {
    roleCount: number
};

export type DetailedProject = Project & {
    roles: Role[],
    devices: Device[],
};

export type User = {
    id: number,
    username: string,
    first_name?: string,
    last_name?: string,
};

export const PERMISSION_KEYS = [
    'allow_publish',
    'allow_subscribe',
    'allow_qos0',
    'allow_qos1',
    'allow_qos2',
    'allow_retained',
    'allow_shared_subscriptions'
] as const;

export type KeyOfPermission = typeof PERMISSION_KEYS[number];

export type RolePermissions = { [key in KeyOfPermission]?: boolean } & {
    id: number,
    topic: string,
    shared_group: string,
    role: number,
};

export type Role = {
    id: number,
    name: string,
    description: string,
    project: number, // only id
    devices: number[], // only id each
    permissions: RolePermissions[],
};

export type Device = {
    id: number,
    name: string,
    roles: number[],
    project: number,
    password?: string, // this password is allowed to be transferred plain-text
};

export type NewDevice = Omit<Device, "id">;
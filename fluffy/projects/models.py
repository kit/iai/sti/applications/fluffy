"""Contains the models for the mqtt app."""
from django.db import models
from django.conf import settings

# Create your models here.


class Project(models.Model):
    """Describes a project.

    Fields:
        name: The unique name of the project
        managers: Users which are allowed to manage the project
        members: Users which are only users of the project
    """

    name = models.CharField(max_length=64, verbose_name="Name", unique=True)
    managers = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name="Managers", related_name="+")
    members = models.ManyToManyField(settings.AUTH_USER_MODEL, verbose_name="Members", related_name="+")

    def __str__(self):
        """Return the name of the model.

        Returns:
            name field
        """
        return f"{self.name}"

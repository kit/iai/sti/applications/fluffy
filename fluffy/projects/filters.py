"""Contains object level filters for views."""
from rest_framework.filters import BaseFilterBackend
from django.db.models import Q  # noqa: WPS347

import logging
logger = logging.getLogger(__name__)


class ProjectListFilter(BaseFilterBackend):
    """Returns only Projects where the user is in the managers or members field."""

    def filter_queryset(self, request, queryset, view):
        """Return only projects where user is member or manager.

        Args:
            request (object): django rest framwork request
            queryset (object): django queryset
            view (object): the view from which this filter is called

        Returns:
            filtered queryset
        """
        user = request.user
        if request.user.is_superuser:
            return queryset.all()
        return queryset.filter(Q(managers=user) | Q(members=user)).distinct()

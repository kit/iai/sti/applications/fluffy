"""Test the project model"""

from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
from django.test import TestCase, override_settings
from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase
from django.utils.text import slugify

from projects.models import Project

User = get_user_model()

faker = Faker()


class TestFields(TestCase):
    def test_name_field_unique(self):
        with self.assertRaises(IntegrityError):
            Project.objects.create(name="test-project")
            Project.objects.create(name="test-project")


class TestStrFunction(TestCase):
    def test_str_function(self):
        test_project = Project.objects.create(name=faker.word())
        self.assertEqual("{0}".format(test_project.name), test_project.__str__())


@override_settings(HIVEMQ_NO_SYNC=True)
class TestApiLogic(APITestCase):
    def setUp(self) -> None:
        self._url = reverse("project-list")
        self.test_user = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_2 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.project = Project.objects.create(name=faker.word())
        self.project.managers.add(self.test_user)
        self.project.members.add(self.test_user_2)
        self.project.save()

    def post_fake_project(self, user, **kwargs):
        self.client.force_login(user)
        data = {
            "name": faker.word(),
            **kwargs,
        }
        return self.client.post(self._url, data)

    def test_creator_to_manager(self):
        """Whoever creates a project, should be manager"""
        response = self.post_fake_project(self.test_user)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        project = Project.objects.get(id=response.data["id"])
        self.assert_(
            project.managers.filter(id=self.test_user.id).exists(),
            "Creator is not manager of the created project",
        )

    def test_creator_to_manager_only(self):
        """A request to create a project with given manager list is ignored
        i.e. a project is always created with exactly the creator as manager
        """
        response = self.post_fake_project(
            user=self.test_user,
            managers=[self.test_user.id, self.test_user_2.id],
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        project = Project.objects.get(id=response.data["id"])
        self.failUnless(
            project.managers.filter(id=self.test_user.id).exists(),
            "Creator is not manager of the created project",
        )
        self.failIf(
            project.managers.filter(id=self.test_user_2.id).exists(),
            "Non-Creator User became manager of the created project",
        )

    def test_rootTopic_autocreate(self):
        """The auto-created roottopic is specified to be the slugified project name"""
        response = self.post_fake_project(self.test_user)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        project = Project.objects.get(id=response.data["id"])
        self.assertEqual(project.roottopic.topic, slugify(project.name))


@override_settings(HIVEMQ_NO_SYNC=True)
class TestPermissions(APITestCase):
    def setUp(self) -> None:
        self._url = reverse("project-list")
        self.test_user = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_2 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_superuser = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_superuser.is_superuser = True
        self.test_superuser.save()

    def test_create_user(self):
        self.client.force_login(self.test_user)
        response = self.client.post(
            self._url,
            data={
                "name": faker.word()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_superuser(self):
        self.client.force_login(self.test_superuser)
        response = self.client.post(
            self._url,
            data={
                "name": faker.word()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_manager(self):
        test_project = Project.objects.create(name=faker.word())
        test_project.managers.add(self.test_user)
        test_project.save()
        self.client.force_login(self.test_user)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_project.id),
            data={
                "name": new_name
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Project.objects.get(id=test_project.id).name, new_name)

    def test_update_member(self):
        test_project = Project.objects.create(name=faker.word())
        test_project.members.add(self.test_user)
        test_project.save()
        self.client.force_login(self.test_user)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_project.id),
            data={
                "name": new_name
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Project.objects.get(id=test_project.id).name, test_project.name)

    def test_update_non_member(self):
        test_project = Project.objects.create(name=faker.word())
        self.client.force_login(self.test_user)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_project.id),
            data={
                "name": new_name
            }
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(Project.objects.get(id=test_project.id).name, test_project.name)

    def test_update_superuser(self):
        test_project = Project.objects.create(name=faker.word())
        self.client.force_login(self.test_superuser)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_project.id),
            data={
                "name": new_name
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Project.objects.get(id=test_project.id).name, new_name)

    def test_delete_manager(self):
        test_project = Project.objects.create(name=faker.word())
        test_project.managers.add(self.test_user)
        self.client.force_login(self.test_user)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_project.id)
        )
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        with self.assertRaises(ObjectDoesNotExist):
            Project.objects.get(id=test_project.id)

    def test_delete_member(self):
        test_project = Project.objects.create(name=faker.word())
        test_project.members.add(self.test_user)
        self.client.force_login(self.test_user)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_project.id)
        )
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_delete_non_member(self):
        test_project = Project.objects.create(name=faker.word())
        self.client.force_login(self.test_user)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_project.id)
        )
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_delete_superuser(self):
        test_project = Project.objects.create(name=faker.word())
        self.client.force_login(self.test_superuser)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_project.id)
        )
        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        with self.assertRaises(ObjectDoesNotExist):
            Project.objects.get(id=test_project.id)

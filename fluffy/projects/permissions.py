"""Contains permission classes."""
from rest_framework import permissions


class ProjectPermissions(permissions.DjangoObjectPermissions):
    """Permissions class for the django rest framework.

    * Superusers can do everything
    * Every Authenticated user is allowed to create a project
    * only project managers are allowed to change
    * members can only view
    """

    def has_permission(self, request, view):
        """Check if the User has the permission for a non object related method.

        Args:
            request (object): rest framework request
            view (object): The view from which the filter is called

        Returns:
            True if user is authenticated
        """
        return request.user.is_authenticated

    def has_object_permission(self, request, view, request_object):
        """Check if the User has the permission for an object related method. Allow write methods only for managers and superusers.

        Args:
            request (object): rest framework request
            view (object): The view from which the filter is called
            request_object (object): The object that should be accessed

        Returns:
            True if user is a superuser or request method is safe or delete
        """
        if request.user.is_superuser:
            return True

        if (  # noqa: WPS337
            request.user in request_object.members.all()
            and request.method in permissions.SAFE_METHODS
        ):
            return True

        return request.user in request_object.managers.all()

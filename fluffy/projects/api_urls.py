"""Contains the api endpoints for the mqtt app."""
from rest_framework import routers
from projects.views import ProjectViewSet, UserViewSet
from django.urls import path, include

router = routers.SimpleRouter()
router.register("projects", viewset=ProjectViewSet)
router.register("users", viewset=UserViewSet)

urlpatterns = [
    path("", include(router.urls)),
]

from django.db.models import Q
from django.contrib.auth import get_user_model
from ldap import NO_UNIQUE_ENTRY, NO_SUCH_OBJECT

from projects.ldap_users import query_ldap_whole_staff_for

User = get_user_model()


def get_or_create_user(username) -> User:
    """
    Logic for adding a user to a project:
    1. do they already exist in our database? -> just return his id
    2. is just username given? -> try to retrieve information via LDAP
       -> if that exists -> create new entry with that
    2b) whole info given or LDAP doesn't exist -> create new entry with given information
    """
    lookup_user = User.objects.filter(Q(username=username))
    if lookup_user.exists():
        return lookup_user.first()

    ldap_users = query_ldap_whole_staff_for(username)
    if not ldap_users:
        raise NO_SUCH_OBJECT
    if len(ldap_users) > 1:
        raise NO_UNIQUE_ENTRY
    ldap_user = ldap_users[0]
    return User.objects.create(
        username=username,
        first_name=ldap_user.get("givenName", ""),
        last_name=ldap_user.get("sn", ""),
        email=ldap_user["userPrincipalName"],
        is_active=False,
        is_staff=False,
    )

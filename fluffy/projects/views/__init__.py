"""Contains the views for the projects app."""
from .project import ProjectViewSet
from .user import UserViewSet

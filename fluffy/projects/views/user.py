import logging
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from django.contrib.auth import get_user_model

from projects.serializers import UserSerializer


User = get_user_model()

logger = logging.getLogger(__name__)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """viewset for the user model."""

    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=False)
    def me(self, request):
        """Return the user object for the actual User.

        Args:
            request (object): django rest framework request

        Returns:
            django rest framework response
        """
        serializer = self.get_serializer(request.user, many=False)
        return Response(serializer.data)

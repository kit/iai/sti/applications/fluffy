import traceback

import ldap
from django.contrib.auth import get_user_model
from django.db import IntegrityError
from django.utils.text import slugify
from ldap import NO_UNIQUE_ENTRY, NO_SUCH_OBJECT
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from fluffy.settings import LDAP_SEARCH_BASE, LDAP_SEARCH_FILTER, LDAP_HOST_URL
from fluffy.utils import get_message_from_validation_error, get_message_from_integrity_error
from mqtt.models import RootTopic, Role
from projects.filters import ProjectListFilter
from projects.ldap_users import query_ldap_users
from projects.models import Project
from projects.permissions import ProjectPermissions
from projects.serializers import ProjectSerializer
from projects.serializers.project import ProjectDetailsSerializer
from projects.user_logic import get_or_create_user

import logging


logger = logging.getLogger(__name__)

User = get_user_model()


class ProjectViewSet(viewsets.ModelViewSet):
    """viewset for the project model."""

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    permission_classes = [ProjectPermissions]
    filter_backends = [ProjectListFilter]

    def get_serializer_class(self):
        if self.action == "retrieve":
            return ProjectDetailsSerializer
        else:
            return ProjectSerializer

    def perform_create(self, serializer):
        patched_managers = serializer.validated_data.get("managers", [])
        patched_managers.append(serializer.context["request"].user.id)
        serializer.save(managers=patched_managers)

    def create(self, request, *args, **kwargs):
        if "roottopic" in request.data:
            # incoming "roottopic" will be ignored, we create it automatically.
            request.data.pop("roottopic")

        try:
            response = super().create(request, args, kwargs)
        except ValidationError as exc:
            message = get_message_from_validation_error(exc)
            return Response(data=message, status=status.HTTP_409_CONFLICT)
        except IntegrityError as exc:
            message = get_message_from_integrity_error(exc)
            print(traceback.format_exc())
            return Response(data=message, status=status.HTTP_409_CONFLICT)

        if response.status_code == status.HTTP_201_CREATED:
            return self.response_after_ensuring_consistency(response)
        else:
            return response

    def response_after_ensuring_consistency(self, response):
        root_topic = slugify(response.data["name"])
        created_project = Project.objects.get(id=response.data["id"])
        try:
            RootTopic.objects.create(topic=root_topic, project_id=created_project.id)
        except IntegrityError:
            created_project.delete()
            return Response(
                data=f"Project cannot have that name because its roottopic \"/{root_topic}\" already exists.",
                status=status.HTTP_409_CONFLICT,
            )
        serializer = ProjectSerializer(
            Project.objects.get(id=response.data["id"]),
            context={
                "request": response.data.serializer.context["request"]
            }
        )
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data,
            status=status.HTTP_201_CREATED,
            headers=headers,
        )

    def list(self, request, *args, **kwargs):
        raw_response = super().list(request, *args, **kwargs)
        enriched_data = [
            self.enrich_for_overview(project, request.user)
            for project in raw_response.data
        ]
        return Response(
            data=enriched_data,
            status=status.HTTP_200_OK,
        )

    @staticmethod
    def enrich_for_overview(project, user):
        def user_is_in(field):
            return next((m for m in field if m["username"] == user.username), None) is not None

        project["isSuperUser"] = user.is_superuser
        project["isManager"] = user_is_in(project["managers"])
        project["isMember"] = user_is_in(project["members"])
        project["canEditMembers"] = project["isSuperUser"] or project["isManager"]

        roles = Role.objects.filter(pk=project["id"])
        project["roleCount"] = len(roles)

        return project

    @action(url_path="ldap-users", detail=False)
    def get_ldap_user_list(self, _request, *_args, **_kwargs):
        try:
            return Response(query_ldap_users())
        except ldap.SERVER_DOWN as exc:
            if not LDAP_HOST_URL:
                return Response(
                    status=status.HTTP_404_NOT_FOUND,
                    data="environment variable LDAP_HOST_URL is unspecified.",
                )
            else:
                return Response(
                    status=status.HTTP_503_SERVICE_UNAVAILABLE,
                    data=exc.args,
                )
        except ldap.SIZELIMIT_EXCEEDED as exc:
            logger.warning(f"Size limit exceeded, {LDAP_SEARCH_BASE=}, {LDAP_SEARCH_FILTER=}")
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=exc.args)
        except Exception as exc:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=exc.args)

    @action(methods=["PATCH"], detail=True, url_path="user/(?P<user_id>\\d+)/toggle-manager")
    def toggle_user_role(self, _request, *_args, **kwargs):
        project_id = kwargs.get("pk")
        user_id = kwargs.get("user_id")
        project = Project.objects.get(pk=project_id)

        # we check both to sanitize potential data inconsistencies
        is_manager = project.managers.filter(pk=user_id).exists()
        is_member = project.members.filter(pk=user_id).exists()
        if is_manager and is_member:
            # if both are accidentally set, the intention should be to keep manager status
            project.members.remove(user_id)
        elif is_manager:
            project.managers.remove(user_id)
            project.members.add(user_id)
        elif is_member:
            project.managers.add(user_id)
            project.members.remove(user_id)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

        project.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=["DELETE"], detail=True, url_path="user/(?P<user_id>\\d+)")
    def remove_user(self, _request, *_args, **kwargs):
        project_id = kwargs.get("pk")
        user_id = kwargs.get("user_id")
        project = Project.objects.get(pk=project_id)
        project.members.remove(user_id)
        project.managers.remove(user_id)
        project.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @action(methods=["POST"], detail=True, url_path="add-user")
    def add_member(self, request, *_args, **kwargs):
        username = request.data.get("username")
        if not username:
            return Response(
                data="No KIT username given",
                status=status.HTTP_422_UNPROCESSABLE_ENTITY,
            )
        try:
            user = get_or_create_user(username)
        except NO_UNIQUE_ENTRY:
            return Response(
                data=f"Given username does not match a unique KIT user: {username}",
                status=status.HTTP_409_CONFLICT,
            )
        except NO_SUCH_OBJECT:
            return Response(
                data=f"No corresponding user found in KIT LDAP: {username}",
                status=status.HTTP_404_NOT_FOUND,
            )
        project = Project.objects.get(pk=kwargs["pk"])
        project.members.add(user.id)
        project.save()
        return Response(data=user.id, status=status.HTTP_201_CREATED)

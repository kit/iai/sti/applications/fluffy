"""Contains the config for the projects app."""
from django.apps import AppConfig


class ProjectsConfig(AppConfig):
    """App Configuration Class.

    see https://docs.djangoproject.com/en/4.0/ref/applications/#configuring-applications
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "projects"

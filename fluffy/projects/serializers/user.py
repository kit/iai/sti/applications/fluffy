
from rest_framework import serializers
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    """Serializer for the user model."""

    class Meta:  # noqa: WPS306
        """Metadata for the serializer."""

        model = User
        fields = ["id", "username", "first_name", "last_name"]


"""Contains the serializers for the djangorestframework."""
from .project import ProjectSerializer
from .user import UserSerializer

"""Contains the serializers for the djangorestframework."""

from rest_framework import serializers

from mqtt.serializers import RoleSerializer, DeviceSerializer
from projects.models import Project
from .user import UserSerializer

from django.contrib.auth import get_user_model

User = get_user_model()


class ProjectSerializer(serializers.ModelSerializer):
    """Serializer for the project model on the overview level."""

    managers = UserSerializer(
        instance=User.objects.all(),
        many=True,
        required=False,
        read_only=True
    )
    members = UserSerializer(
        instance=User.objects.all(),
        many=True,
        required=False,
        read_only=True
    )
    roottopic = serializers.StringRelatedField(
        many=False,
        required=False,
    )

    class Meta:  # noqa: WPS306
        model = Project
        fields = [
            "id",
            "name",
            "roottopic",
            "managers",
            "members",
        ]


class ProjectDetailsSerializer(ProjectSerializer):
    """Serializer for the project model at detail level."""

    roles = RoleSerializer(
        source='role_set',
        many=True,
        read_only=True,
        required=False
    )
    devices = DeviceSerializer(
        many=True,
        read_only=True,
        source='device_set',
        required=False
    )

    class Meta:  # noqa: WPS306
        model = Project
        fields = [
            *ProjectSerializer.Meta.fields,
            "roles",
            "devices",
        ]

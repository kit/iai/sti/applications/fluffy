import ldap
import logging

from fluffy.settings import LDAP_BIND_DN, LDAP_BIND_PW, LDAP_SEARCH_BASE, LDAP_SEARCH_FILTER, LDAP_HOST_URL

logger = logging.getLogger(__name__)

ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)

filter_attrlist = [
    "displayname",
    "samaccountname",
    "userprincipalname",
    "sn",
    "givenName",
    "department"
]

def open_ldap_connection(log=False):
    connection = ldap.initialize(LDAP_HOST_URL)
    connection.set_option(ldap.OPT_REFERRALS, 0)
    connection.set_option(ldap.OPT_DEBUG_LEVEL, 255)
    if log:
        logger.debug("LDAP Connection initialzed.")
    connection.simple_bind_s(LDAP_BIND_DN, LDAP_BIND_PW)
    if log:
        logger.debug(f"LDAP Connection bound: {connection}")
    return connection


def query_ldap_whole_staff_for(username):
    try:
        filterstr = f"(|(sAMAccountName={username})(cn={username}))"
        connection = open_ldap_connection(log=True)
        search = connection.search_ext_s(
            base="ou=Staff,ou=KIT,dc=kit,dc=edu",
            scope=ldap.SCOPE_SUBTREE,
            filterstr=filterstr,
            attrlist=filter_attrlist
        )
        return decoded_search_results(search)
    except Exception:
        return []


def query_ldap_users(ignore_errors=False):
    try:
        connection = open_ldap_connection(log=True)
        search = connection.search_ext_s(
            base=LDAP_SEARCH_BASE,
            scope=ldap.SCOPE_SUBTREE,
            filterstr=LDAP_SEARCH_FILTER,
            attrlist=filter_attrlist
        )
        return decoded_search_results(search)
    except Exception as e:
        if ignore_errors:
            return []
        raise e


def decoded_search_results(original_results):
    decoded_results = []
    for cn, entry in original_results:
        decoded_entry = {"cn": cn}
        for key in entry.keys():
            decoded_entry[key] = entry[key][0].decode("utf-8")
        decoded_results.append(decoded_entry)
    return decoded_results


def query_ldap_user_by_key(key, username):
    users = query_ldap_users(ignore_errors=True)
    try:
        return next(user
                    for user in users
                    if user[key] == username)
    except StopIteration:
        return None
    except KeyError:
        return None

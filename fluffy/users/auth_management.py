from django.contrib.auth.base_user import BaseUserManager


def extend_user(user, claims):
    """
    OIDC will enter the eduperson_principal_name [ab1234@kit.edu or similar] as a username.

    This is incompatible with LDAP, so we have use preferred_username.
    There is also no need to store any password on our own, it is all handled by OIDC.
    """
    user.email = claims["eduperson_principal_name"]
    user.username = claims["preferred_username"]
    user.password = ""  # noqa: S105
    return user


def username_func(claims):
    """This is the LDAP field we compare to"""
    return claims["preferred_username"]


class KitUserManager(BaseUserManager):
    """
    A custom user manager with minimal duplication of the KIT LDAP user information.

    This project uses a custom user manager (*) because authentication has to match the user base present at KIT.
    The current way includes
    1) any user can authenticate via OIDC using the frontend (via redirect to KIT OIDC and back)
    2) after authentication, the user row exists in the database (auth_user table or similar) with is_active = True
    3) by adding a user to a project, (who can be found via request to the KIT LDAP)
    3a) if this user has a db entry, the corresponding user entry will be linked to the project
    3b) if not, a new entry will be added to the db with is_active flag set to False
    4) if, later, this user authenticates via OIDC, its is_active will be set to True

    the column "is_superuser" is ignored because the single superuser is defined via SUPERUSER_NAME env var
    Also, we do not store any passwords or their hashes in our database, there is no need for that.

    (*) do not confuse this concept of a user manager with the "manager" rule a user can have for a specific project.
    """

    def create_user(self, username, _password, **extra_fields):
        user = self.model(username=username, **extra_fields)
        user.set_password("")
        user.is_superuser = False
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        """
        cf. above, there is no use in the extra superuser role as intended by the framework.
        because the single superuser is defined via env var SUPERUSER_NAME = "ab1234"
        i.e. it is specified by the corresponding environment admin.
        """
        return self.create_user(username, password, **extra_fields)

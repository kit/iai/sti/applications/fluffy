from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import KitUser


class KitUserAdmin(UserAdmin):
    model = KitUser
    list_display = ["email", "username", "is_staff"]


admin.site.register(KitUser, KitUserAdmin)

"""
This project does not define own users but relies on the KIT userbase.
Therefore, our user entities only need to hold selected pieces of metainformation,
especially not the actual login credentials.
"""

from django.contrib.auth.models import AbstractUser
from django.db import models

from users.auth_management import KitUserManager


class KitUser(AbstractUser):
    # the username will be the unique KIT ab1234 identifier
    # (we do not limit the max_length to 6 in order not to complicate any future changes)
    username = models.CharField("username", max_length=100, unique=True)

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    objects = KitUserManager()

    def __str__(self):
        """simplified for clearer logging"""
        return self.username

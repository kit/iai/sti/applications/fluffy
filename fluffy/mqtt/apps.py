"""This is the app configuration for the mqtt module."""

from django.apps import AppConfig
import sys

class MqttConfig(AppConfig):
    """App Configuration Class.

    see https://docs.djangoproject.com/en/4.0/ref/applications/#configuring-applications
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "mqtt"

    def ready(self):
        if "runserver" in sys.argv:
            self.startup_on_runserver()

    @staticmethod
    def startup_on_runserver():
        from mqtt.hivemq_sync import update_hivemq_db
        import pymysql.err as hivemq_related
        import django.db.utils as django_related

        try:
            update_hivemq_db()
        except hivemq_related.OperationalError:  # hivemq not accessible
            pass
        except django_related.OperationalError:  # local DB might not exist yet
            pass

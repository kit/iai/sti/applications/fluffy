"""Contains the data models for the mqtt app."""
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models

from mqtt.choices import PW_HASH_ALGORITHMS
from mqtt.password_handling import hash_password, generate_salt
from mqtt.validators import mqtt_topic_validator
from projects.models import Project
import logging

logger = logging.getLogger(__name__)


class Role(models.Model):
    """Describes a mqtt device role.

    Name and Project together need to be unique

    Fields:
        name: The name of the role
        description: The description of the role
        project: The related project
    """

    name = models.CharField(
        max_length=256,
        verbose_name="Name",
    )
    description = models.TextField(
        verbose_name="Description"
    )
    project = models.ForeignKey(
        Project,
        verbose_name="Project",
        null=False,
        on_delete=models.CASCADE,
    )

    class Meta:  # noqa: WPS306
        """Metadata options for the Role model."""

        constraints = [
            models.UniqueConstraint(fields=["name", "project"], name="full_role_name")
        ]

    def __str__(self):
        """Return the name of the model.

        Returns:
            name field
        """
        return "{0} ({1})".format(self.name, self.project)


class Device(models.Model):
    """Describes a mqtt device.

    Fields
        name: The username of the device
        password: The password for the MQTT Device (base64 encoded)
        pw_hash_algo: The algorithm which is used for generation password
        pw_salt: The salt with which the password was salted
        roles: Roles to which the device is linked
        project: Project which the device belongs to
    """

    name = models.CharField(max_length=64, verbose_name="Name", unique=True)
    password = models.CharField(max_length=255, verbose_name="Password")
    pw_hash_algorithm = models.CharField(
        choices=PW_HASH_ALGORITHMS,
        max_length=10,
    )
    pw_hash_rounds = models.IntegerField(verbose_name="Hash rounds", default=0)
    pw_salt = models.CharField(max_length=255, verbose_name="Password Salt")
    roles = models.ManyToManyField(Role, verbose_name="Roles")
    project = models.ForeignKey(
        Project,
        verbose_name="Project",
        null=False,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        """Return the name of the model.

        Returns:
            name + dev_id field
        """
        return "{0} ({1})".format(self.name, self.project.name)

    def save(self, *args, **kwargs):
        """Handle the object save logic.

        Args:
            *args: fields
            **kwargs: fields
        """

        if kwargs.get("force_insert"):
            self.password = self._hash_password(password=self.password)
        elif kwargs.get("force_update"):
            self.password = self._hash_password(password=self.password, keep_salt=True)

        super(Device, self).save(*args, **kwargs)  # noqa: WPS608

    def _hash_password(self, password: str, keep_salt: bool = False):
        """Update the password fields.

        Args:
            password (str): The new password

        Returns:
            hashed password
        """

        if not keep_salt:
            self.pw_salt = generate_salt()
            self.pw_hash_algorithm = settings.MQTT["HASH_ALGORITHM"]
            self.pw_hash_rounds = settings.MQTT["HASH_ROUNDS"]

        pw_hash = hash_password(password, self.pw_salt, self.pw_hash_rounds, self.pw_hash_algorithm)
        return pw_hash


class Permission(models.Model):
    """Describes a mqtt permission.

    Fields:
        topic: The topic expression for this permission (see https://www.hivemq.com/docs/ese/4.7/enterprise-security-extension/ese.html#permission-placeholders)
        allow_publish: Allow publishing
        allow_subscribe: Allow subscribing
        allow_qos0: Allow qos0 messages
        allow_qos1: Allow qos1 messages
        allow_qos2: Allow qos2 messages
        allow_retained: Allow retained messages
        allow_shared_subscriptions: Allow shared subscriptions
        shared_group: The group for the shard subscriptions
        role: Role to which this permission is linked
    """

    topic = models.CharField(
        max_length=512,
        unique=True,
        verbose_name="Topic Expression",
        validators=[mqtt_topic_validator],
    )
    allow_publish = models.BooleanField(verbose_name="Allow Publish", default=False)
    allow_subscribe = models.BooleanField(verbose_name="Allow Subscribe", default=False)
    allow_qos0 = models.BooleanField(verbose_name="Allow QoS0", default=False)
    allow_qos1 = models.BooleanField(verbose_name="Allow QoS1", default=False)
    allow_qos2 = models.BooleanField(verbose_name="Allow QoS2", default=False)
    allow_retained = models.BooleanField(
        verbose_name="Allow retained messages",
        default=False,
    )
    allow_shared_subscriptions = models.BooleanField(
        verbose_name="Allow shared subscriptions",
        default=False,
    )
    shared_group = models.CharField(
        verbose_name="Shared Group",
        max_length=64,
        blank=True,
    )
    role = models.ForeignKey(Role, models.CASCADE, verbose_name="Role")

    def __str__(self):
        """Return the name of the model.

        Returns:
            topic field
        """
        return f"{self.topic}"

    def clean(self):
        """Validate the model fields.

        Raises:
            ValidationError: If validation fails
        """
        super(Permission, self).clean()
        errors = {}
        if not self.topic.startswith(self.role.project.roottopic.topic):
            errors[
                "root_topic"
            ] = "The permission need to start with the project root topic!"
        if self.allow_shared_subscriptions and self.shared_group == "":
            errors[
                "allow_shared_subscriptions"
            ] = "Please set a shared group if you want to enable shared Subscriptions"
        if errors:
            raise ValidationError(errors)


class RootTopic(models.Model):
    """Describes a mqtt root topic.

    Fields:
    topic: The mqtt root topic
    projects: The relation to the project model
    """

    topic = models.CharField(
        max_length=512,
        verbose_name="Root Topic",
        unique=True,
        validators=[mqtt_topic_validator],
    )
    project = models.OneToOneField(Project, on_delete=models.CASCADE)

    def __str__(self):
        """Return the name of the model.

        Returns:
            topic field
        """
        return f"{self.topic}"

from django.core.management.base import BaseCommand
from django.conf import settings
from mqtt.hivemq_sync import connect_hivemq, truncate_db, update_db


class Command(BaseCommand):
    help = "Remove all role content of the HiveMQ database and re-initialize from local sqlite"

    def handle(self, *args, **options):
        mqtt_settings = settings.MQTT["HIVE_MQ_DB"]
        with connect_hivemq(mqtt_settings) as connection:
            truncate_db(connection, verbose=True)
            update_db(connection, verbose=True)
        self.stdout.write("Command finished.")

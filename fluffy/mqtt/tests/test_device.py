"""Test the device model"""

from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
from django.test import TestCase, override_settings
from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

from mqtt.models import Device
from mqtt.password_handling import hash_password
from projects.models import Project

from parameterized import parameterized

import logging
from faker import Faker

logger = logging.getLogger(__name__)
faker = Faker()
User = get_user_model()


class TestFields(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())
        self.project_2 = Project.objects.create(name=faker.word())

    def test_name_field_unique(self):
        with self.assertRaises(IntegrityError):
            Device.objects.create(name="test-device", project=self.project_1)
            Device.objects.create(name="test-device", project=self.project_2)

    def test_password_field_hashing(self):
        password = faker.password()
        device = Device.objects.create(
            name="test-device", project=self.project_1, password=password
        )
        device.save()
        self.assertNotEqual(password, device.password)

    def test_project_field_cascade(self):
        project_3 = Project.objects.create(name=faker.word())
        test_device_1 = Device.objects.create(name=faker.word(), project=project_3)
        Device.objects.get(id=test_device_1.id)
        project_3.delete()
        with self.assertRaisesMessage(
            ObjectDoesNotExist, "Device matching query does not exist."
        ):
            Device.objects.get(id=test_device_1.id)


class TestPasswordHashCase(TestCase):
    """Test the password hashing."""

    @parameterized.expand(
        [
            (
                "SHA512 One Round",
                "test",
                "",
                1,
                "sha512",
                "7iaw3Ur350mqGo7jwQrpkj9hiYB3Lkc/iBml1JQODbJ6wYX4oOHV+E+IvIh/1nsUNzLDBMxfqa2Ob1f1ACio/w==",
            ),
            (
                "SHA512 Ten Rounds",
                "test",
                "",
                10,
                "sha512",
                "rAgGSI9KuJd/VY7JX49w9hIESUYkp38Obcvn8zVIHTTClfGiKD0K9XB6zl7+AhVLu0H5FcNf0II1MFB2RL9UJQ==",
            ),
            (
                "SHA512 100 Rounds with Salt",
                "test",
                "pepper",
                100,
                "sha512",
                "BcwvDIe4yu9iwlVEvja/hvqS2JIaFo81qjuyDDxgJYZBSO/icaobHrkg3NhTLnVf9h1EufuJNmna1tmH27GWEA==",
            ),
        ],
    )
    def test_password_hashing(self, _, password, salt, rounds, algorithm, hashed):
        """Test the password hashing function with different cases.

        Args:
            password (str): A password
            salt (str): A salt
            rounds (int): Hashing rounds
            algorithm (str): The hashing algorithm (only sha512 implemented)
            hashed (str): The hashed version of the algorithm(password + salt) x rounds
        """
        result = hash_password(password, salt, rounds, algorithm)
        self.assertEqual(hashed, result)

    def test_invalid_hash_function(self):
        """Test if an invalid hash function raises an Error."""
        with self.assertRaises(NotImplementedError):
            hash_password("abcde", "fghij", 10, "md5")


class TestStrFunction(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())

    def test_str_function(self):
        test_device_1 = Device.objects.create(name=faker.word(), project=self.project_1)
        self.assertEqual(
            "{0} ({1})".format(test_device_1.name, test_device_1.project.name),
            test_device_1.__str__(),
        )


@override_settings(HIVEMQ_NO_SYNC=True)
class TestPermissions(APITestCase):
    def setUp(self) -> None:
        self._url = reverse("device-list")
        self.test_user_1 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_2 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3.is_superuser = True
        self.test_user_3.save()
        self.test_user_4 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.project_1 = Project.objects.create(name=faker.word())
        self.project_1.managers.add(self.test_user_1.id)
        self.project_1.members.add(self.test_user_4.id)
        self.project_1.save()
        self.project_2 = Project.objects.create(name=faker.word())
        self.project_2.managers.add(self.test_user_2.id)
        self.project_2.save()

    def post_fake_device(self, project, user):
        self.client.force_login(user)
        return self.client.post(
            self._url,
            data={
                "name": faker.word(),
                "project": project.id,
            },
        )

    def assert_status(self, response, status: int):
        try:
            self.assertEqual(response.status_code, status)
        except AssertionError as exc:
            logger.warning(f"Response Assertion failed: {response.status_code} ({response.content})")
            logger.error(str(exc))
            raise exc

    def test_create_with_project_manager(self):
        response = self.post_fake_device(self.project_1, self.test_user_1)
        self.assert_status(response, status.HTTP_201_CREATED)

    def test_create_with_project_member(self):
        response = self.post_fake_device(self.project_1, self.test_user_4)
        self.assert_status(response, status.HTTP_201_CREATED)

    def test_create_with_non_project_member(self):
        response = self.post_fake_device(self.project_1, self.test_user_2)
        self.assert_status(response, status.HTTP_403_FORBIDDEN)

    def test_create_with_superuser(self):
        response = self.post_fake_device(self.project_1, self.test_user_3)
        self.assert_status(response, status.HTTP_201_CREATED)

    def put_fake_device(self, project, user):
        test_device = Device.objects.create(name=faker.word(), project=project)
        test_device.save()
        self.client.force_login(user)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_device.id),
            data={
                "name": "{0}".format(new_name),
                "project": project.id,
                "password": faker.password(10)
            },
        )
        return new_name, test_device, response

    def test_update_with_project_manager(self):
        new_name, test_device, response = self.put_fake_device(
            self.project_1,
            self.test_user_1
        )
        self.assert_status(response, status.HTTP_200_OK)
        self.assertEqual(new_name, Device.objects.get(id=test_device.id).name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_device.id),
            data={"name": "{0}".format(new_name)},
        )
        self.assert_status(response, status.HTTP_200_OK)
        self.assertEqual(new_name, Device.objects.get(id=test_device.id).name)

    def test_update_with_project_member(self):
        new_name, test_device, response = self.put_fake_device(
            self.project_1,
            self.test_user_4
        )
        self.assert_status(response, status.HTTP_200_OK)
        self.assertEqual(new_name, Device.objects.get(id=test_device.id).name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_device.id),
            data={"name": "{0}".format(new_name)},
        )
        self.assert_status(response, status.HTTP_200_OK)
        self.assertEqual(new_name, Device.objects.get(id=test_device.id).name)

    def test_update_with_non_project_member(self):
        new_name, test_device, response = self.put_fake_device(
            self.project_1,
            self.test_user_2
        )
        self.assert_status(response, status.HTTP_403_FORBIDDEN)
        self.assertEqual(test_device.name, Device.objects.get(id=test_device.id).name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_device.id),
            data={"name": "{0}".format(new_name)},
        )
        self.assert_status(response, status.HTTP_403_FORBIDDEN)
        self.assertEqual(test_device.name, Device.objects.get(id=test_device.id).name)

    def test_update_with_superuser(self):
        new_name, test_device, response = self.put_fake_device(
            self.project_1,
            self.test_user_3
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(new_name, Device.objects.get(id=test_device.id).name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_device.id),
            data={"name": "{0}".format(new_name)},
        )
        self.assert_status(response, status.HTTP_200_OK)
        self.assertEqual(new_name, Device.objects.get(id=test_device.id).name)

    def test_delete_with_project_manager(self):
        test_device = Device.objects.create(name=faker.word(), project=self.project_1)
        test_device.save()
        self.client.force_login(self.test_user_1)
        response = self.client.delete("{0}{1}/".format(self._url, test_device.id))
        self.assert_status(response, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Device.objects.get(id=test_device.id)

    def test_delete_with_project_member(self):
        test_device = Device.objects.create(name=faker.word(), project=self.project_1)
        test_device.save()
        self.client.force_login(self.test_user_4)
        response = self.client.delete("{0}{1}/".format(self._url, test_device.id))
        self.assert_status(response, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Device.objects.get(id=test_device.id)

    def test_delete_with_non_project_member(self):
        test_device = Device.objects.create(name=faker.word(), project=self.project_1)
        test_device.save()
        self.client.force_login(self.test_user_2)
        response = self.client.delete("{0}{1}/".format(self._url, test_device.id))
        self.assert_status(response, status.HTTP_404_NOT_FOUND)
        self.assertEqual(test_device.name, Device.objects.get(id=test_device.id).name)

    def test_delete_with_superuser(self):
        test_device = Device.objects.create(name=faker.word(), project=self.project_1)
        test_device.save()
        self.client.force_login(self.test_user_3)
        response = self.client.delete("{0}{1}/".format(self._url, test_device.id))
        self.assert_status(response, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Device.objects.get(id=test_device.id)

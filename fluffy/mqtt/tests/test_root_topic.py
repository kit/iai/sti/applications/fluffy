from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db.utils import IntegrityError
from django.test import TestCase, override_settings
from django.urls import reverse
from django.contrib.auth import get_user_model
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase

from mqtt.models import RootTopic
from projects.models import Project

User = get_user_model()

faker = Faker()


class TestFields(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())
        self.project_2 = Project.objects.create(name=faker.word())

    def test_topic_field_unique(self):
        with self.assertRaises(IntegrityError):
            RootTopic.objects.create(topic="this/is/a/test", project=self.project_1)
            RootTopic.objects.create(topic="this/is/a/test", project=self.project_2)

    def test_topic_field_length(self):
        with self.assertRaisesMessage(
            ValidationError,
            "{'topic': ['Ensure this value has at most 512 characters (it has 514).']}",
        ):
            test_root_topic_1 = RootTopic.objects.create(
                topic="{0}".format(faker.pystr(514, 514)), project=self.project_1
            )
            test_root_topic_1.full_clean()

    def test_project_field_one_to_one(self):
        with self.assertRaisesMessage(
            IntegrityError, "UNIQUE constraint failed: mqtt_roottopic.project_id"
        ):
            RootTopic.objects.create(
                topic="{0}".format(faker.pystr(20, 50)), project=self.project_1
            )
            RootTopic.objects.create(
                topic="{0}".format(faker.pystr(20, 50)), project=self.project_1
            )

    def test_project_field_cascade(self):
        project_3 = Project.objects.create(name=faker.word())
        test_root_topic_1 = RootTopic.objects.create(
            topic="{0}".format(faker.pystr(20, 50)), project=project_3
        )
        RootTopic.objects.get(topic=test_root_topic_1.topic)
        project_3.delete()
        with self.assertRaisesMessage(
            ObjectDoesNotExist, "RootTopic matching query does not exist."
        ):
            RootTopic.objects.get(topic=test_root_topic_1.topic)


class TestStrFunction(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())

    def test_str_function(self):
        test_root_topic_1 = RootTopic.objects.create(
            topic="{0}".format(faker.pystr(20, 50)), project=self.project_1
        )
        self.assertEqual(
            "{0}".format(test_root_topic_1.topic), test_root_topic_1.__str__()
        )


@override_settings(HIVEMQ_NO_SYNC=True)
class TestPermissions(APITestCase):
    def setUp(self) -> None:
        self._url = reverse("roottopic-list")
        self.test_user_1 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_2 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3.is_superuser = True
        self.test_user_3.save()
        self.test_user_4 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.project_1 = Project.objects.create(name=faker.word())
        self.project_1.managers.add(self.test_user_1.id)
        self.project_1.members.add(self.test_user_4.id)
        self.project_1.save()
        self.project_2 = Project.objects.create(name=faker.word())
        self.project_2.managers.add(self.test_user_2.id)
        self.project_2.save()

    def test_create_with_project_manager(self):
        self.client.force_login(self.test_user_1)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_project_member(self):
        self.client.force_login(self.test_user_4)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_with_non_project_member(self):
        self.client.force_login(self.test_user_2)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_with_superuser(self):
        self.client.force_login(self.test_user_3)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_with_project_manager(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_1)
        response = self.client.put(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_update_with_project_member(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_4)
        response = self.client.put(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_with_non_project_member(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_2)
        response = self.client.put(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_update_with_superuser(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_3)
        response = self.client.put(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_root_topic.id),
            data={
                "topic": "{0}".format(faker.pystr(20, 50)),
                "project": self.project_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_with_project_manager(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_1)
        response = self.client.delete("{0}{1}/".format(self._url, test_root_topic.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_with_project_member(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_4)
        response = self.client.delete("{0}{1}/".format(self._url, test_root_topic.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_delete_with_non_project_member(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_2)
        response = self.client.delete("{0}{1}/".format(self._url, test_root_topic.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_with_superuser(self):
        test_root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        test_root_topic.save()
        self.client.force_login(self.test_user_1)
        response = self.client.delete("{0}{1}/".format(self._url, test_root_topic.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

"""Test the permissions model."""

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db.utils import IntegrityError
from django.test import TestCase, override_settings
from django.urls import reverse
from django.contrib.auth import get_user_model
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase

from mqtt.models import Permission, Role, RootTopic
from projects.models import Project

User = get_user_model()

faker = Faker()


class TestFields(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())
        self.root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        self.test_role = Role.objects.create(name=faker.word(), project=self.project_1)

    def test_default_values(self):
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(self.root_topic.topic, faker.word()),
            role=self.test_role,
        )
        self.assertEqual(test_permission.allow_publish, False)
        self.assertEqual(test_permission.allow_subscribe, False)
        self.assertEqual(test_permission.allow_qos0, False)
        self.assertEqual(test_permission.allow_qos1, False)
        self.assertEqual(test_permission.allow_qos2, False)
        self.assertEqual(test_permission.allow_retained, False)
        self.assertEqual(test_permission.allow_shared_subscriptions, False)
        self.assertEqual(test_permission.shared_group, "")

    def test_topic_validation(self):
        with self.assertRaises(ValidationError):
            Permission.objects.create(
                topic="{0}/{1}💩".format(self.root_topic.topic, faker.word()),
                role=self.test_role,
            ).full_clean()
            Permission.objects.create(
                topic="abc{0}/{1}".format(self.root_topic.topic, faker.word()),
                role=self.test_role,
            ).full_clean()

    def test_shared_subscription_validation(self):
        with self.assertRaises(ValidationError):
            Permission.objects.create(
                topic="{0}/{1}".format(self.root_topic.topic, faker.word()),
                role=self.test_role,
                allow_shared_subscriptions=True,
            ).full_clean()

    def test_role_field_cascade(self):
        role_2 = Role.objects.create(name=faker.word(), project=self.project_1)
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(self.root_topic.topic, faker.word()), role=role_2
        )
        role_2.delete()
        with self.assertRaisesMessage(
            ObjectDoesNotExist, "Permission matching query does not exist."
        ):
            Permission.objects.get(id=test_permission.id)

    def test_role_required(self):
        with self.assertRaises(IntegrityError):
            Permission.objects.create(
                topic="{0}/{1}".format(self.root_topic.topic, faker.word())
            )


class TestStrFunction(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())
        self.root_topic = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )
        self.test_role = Role.objects.create(name=faker.word(), project=self.project_1)

    def test_str_function(self):
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(self.root_topic.topic, faker.word()),
            role=self.test_role,
        )
        self.assertEqual(
            "{0}".format(test_permission.topic),
            test_permission.__str__(),
        )


@override_settings(HIVEMQ_NO_SYNC=True)
class TestPermissions(APITestCase):
    def setUp(self) -> None:
        self._url = reverse("permission-list")
        self.test_user_1 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_2 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3.is_superuser = True
        self.test_user_3.save()
        self.test_user_4 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.project_1 = Project.objects.create(name=faker.word())
        self.project_1.managers.add(self.test_user_1.id)
        self.project_1.members.add(self.test_user_4.id)
        self.project_1.save()
        self.project_2 = Project.objects.create(name=faker.word())
        self.project_2.managers.add(self.test_user_2.id)
        self.project_2.save()
        self.role_1 = Role.objects.create(
            name=faker.word(),
            project=self.project_1
        )
        self.root_topic_1 = RootTopic.objects.create(
            topic=faker.pystr(20, 50), project=self.project_1
        )

    def test_create_with_project_manager(self):
        self.client.force_login(self.test_user_1)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}/{1}".format(self.root_topic_1.topic, faker.word()),
                "role": self.role_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_project_member(self):
        self.client.force_login(self.test_user_4)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}/{1}".format(self.root_topic_1.topic, faker.word()),
                "role": self.role_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_non_project_member(self):
        self.client.force_login(self.test_user_2)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}/{1}".format(self.root_topic_1.topic, faker.word()),
                "role": self.role_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_with_superuser(self):
        self.client.force_login(self.test_user_3)
        response = self.client.post(
            self._url,
            data={
                "topic": "{0}/{1}".format(self.root_topic_1.topic, faker.word()),
                "role": self.role_1.id,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def _create_and_put(self, root_topic, role, user):
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(root_topic.topic, faker.word()),
            role=role,
        )
        test_permission.save()
        self.client.force_login(user)
        response = self.client.put(
            "{0}{1}/".format(self._url, test_permission.id),
            data={
                "topic": test_permission.topic,
                "role": test_permission.role_id,
                "allow_publish": True,
            },
        )
        return test_permission, response

    def _patch_to_allow_subscribe(self, permission):
        return self.client.patch(
            "{0}{1}/".format(self._url, permission.id),
            data={
                "id": permission.id,
                "topic": permission.topic,
                "role": permission.role_id,
                "allow_subscribe": True,
            },
        )

    def _test_update_with(self, user, expect_allowed):
        expect_status = status.HTTP_200_OK if expect_allowed else status.HTTP_403_FORBIDDEN

        permission, response = self._create_and_put(
            self.root_topic_1,
            self.role_1,
            user
        )
        self.assertEqual(response.status_code, expect_status)
        self.assertEqual(Permission.objects.get(id=permission.id).allow_publish, expect_allowed)

        response = self._patch_to_allow_subscribe(permission)
        self.assertEqual(response.status_code, expect_status)
        self.assertEqual(Permission.objects.get(id=permission.id).allow_subscribe, expect_allowed)

    def test_update_with_project_manager(self):
        self._test_update_with(
            user=self.test_user_1,
            expect_allowed=True
        )

    def test_update_with_project_member(self):
        self._test_update_with(
            user=self.test_user_4,
            expect_allowed=True
        )

    def test_update_with_non_project_member(self):
        self._test_update_with(
            user=self.test_user_2,
            expect_allowed=False
        )

    def test_update_with_superuser(self):
        self._test_update_with(
            user=self.test_user_3,
            expect_allowed=True
        )

    def test_delete_with_project_manager(self):
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(self.root_topic_1.topic, faker.word()),
            role=self.role_1,
        )
        test_permission.save()
        self.client.force_login(self.test_user_1)
        response = self.client.delete("{0}{1}/".format(self._url, test_permission.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Permission.objects.get(id=test_permission.id)

    def test_delete_with_project_member(self):
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(self.root_topic_1.topic, faker.word()),
            role=self.role_1,
        )
        test_permission.save()
        self.client.force_login(self.test_user_4)
        response = self.client.delete("{0}{1}/".format(self._url, test_permission.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Permission.objects.get(id=test_permission.id)

    def test_delete_with_non_project_member(self):
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(self.root_topic_1.topic, faker.word()),
            role=self.role_1,
        )
        test_permission.save()
        self.client.force_login(self.test_user_2)
        response = self.client.delete("{0}{1}/".format(self._url, test_permission.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        Permission.objects.get(id=test_permission.id)

    def test_delete_with_superuser(self):
        test_permission = Permission.objects.create(
            topic="{0}/{1}".format(self.root_topic_1.topic, faker.word()),
            role=self.role_1,
        )
        test_permission.save()
        self.client.force_login(self.test_user_3)
        response = self.client.delete("{0}{1}/".format(self._url, test_permission.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Permission.objects.get(id=test_permission.id)

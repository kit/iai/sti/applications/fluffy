"""Test the role model."""

from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
from django.test import TestCase, override_settings
from django.urls import reverse
from django.contrib.auth import get_user_model
from faker import Faker
from rest_framework import status
from rest_framework.test import APITestCase

from mqtt.models import Role
from projects.models import Project

User = get_user_model()

faker = Faker()


class TestFields(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())
        self.project_2 = Project.objects.create(name=faker.word())

    def test_name_project_unique(self):
        role_name = "Testrole"
        with self.assertRaises(IntegrityError):
            Role.objects.create(name=role_name, project=self.project_1)
            Role.objects.create(name=role_name, project=self.project_1)

    def test_project_field_cascade(self):
        project_3 = Project.objects.create(name=faker.word())
        test_role_1 = Role.objects.create(name=faker.word(), project=project_3)
        Role.objects.get(id=test_role_1.id)
        project_3.delete()
        with self.assertRaisesMessage(
            ObjectDoesNotExist, "Role matching query does not exist."
        ):
            Role.objects.get(id=test_role_1.id)


class TestStrFunction(TestCase):
    def setUp(self) -> None:
        self.project_1 = Project.objects.create(name=faker.word())

    def test_str_function(self):
        test_role_1 = Role.objects.create(name=faker.word(), project=self.project_1)
        self.assertEqual(
            "{0} ({1})".format(test_role_1.name, test_role_1.project.name),
            test_role_1.__str__(),
        )


@override_settings(HIVEMQ_NO_SYNC=True)
class TestPermissions(APITestCase):
    def setUp(self) -> None:
        self._url = reverse("role-list")
        self.test_user_1 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_2 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.test_user_3.is_superuser = True
        self.test_user_3.save()
        self.test_user_4 = User.objects.create(
            username=faker.user_name(), password=faker.password()
        )
        self.project_1 = Project.objects.create(name=faker.word())
        self.project_1.managers.add(self.test_user_1.id)
        self.project_1.members.add(self.test_user_4.id)
        self.project_1.save()
        self.project_2 = Project.objects.create(name=faker.word())
        self.project_2.managers.add(self.test_user_2.id)
        self.project_2.save()

    def test_create_with_project_manager(self):
        self.client.force_login(self.test_user_1)
        response = self.client.post(
            self._url,
            data={
                "name": faker.word(),
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_project_member(self):
        self.client.force_login(self.test_user_4)
        response = self.client.post(
            self._url,
            data={
                "name": faker.word(),
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_with_non_project_member(self):
        self.client.force_login(self.test_user_2)
        response = self.client.post(
            self._url,
            data={
                "name": faker.word(),
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_with_superuser(self):
        self.client.force_login(self.test_user_4)
        response = self.client.post(
            self._url,
            data={
                "name": faker.word(),
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update_with_project_manager(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_1)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Role.objects.get(id=test_role.id).name, new_name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Role.objects.get(id=test_role.id).name, new_name)

    def test_update_with_project_member(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_4)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Role.objects.get(id=test_role.id).name, new_name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Role.objects.get(id=test_role.id).name, new_name)

    def test_update_with_non_project_member(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_2)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Role.objects.get(id=test_role.id).name, test_role.name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Role.objects.get(id=test_role.id).name, test_role.name)

    def test_update_with_superuser(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_3)
        new_name = faker.word()
        response = self.client.put(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Role.objects.get(id=test_role.id).name, new_name)
        new_name = faker.word()
        response = self.client.patch(
            "{0}{1}/".format(self._url, test_role.id),
            data={
                "name": new_name,
                "project": self.project_1.id,
                "description": faker.sentence()
            }
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Role.objects.get(id=test_role.id).name, new_name)

    def test_delete_with_project_manager(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_1)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_role.id),
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Role.objects.get(id=test_role.id)

    def test_delete_with_project_member(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_4)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_role.id),
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Role.objects.get(id=test_role.id)

    def test_delete_with_non_project_member(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_2)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_role.id),
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_with_superuser(self):
        test_role = Role.objects.create(name=faker.word(), project=self.project_1, description=faker.sentence())
        self.client.force_login(self.test_user_3)
        response = self.client.delete(
            "{0}{1}/".format(self._url, test_role.id),
        )
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        with self.assertRaises(ObjectDoesNotExist):
            Role.objects.get(id=test_role.id)

"""Test the mqtt validators."""
from django.test import TestCase
from parameterized import parameterized
from django.core.exceptions import ValidationError
from mqtt.validators import mqtt_topic_validator


class TestMqttTopicValidator(TestCase):
    """Test the password hashing."""

    # 23/01/23: Topics beginning with a slash are forbidden now due to problems in specific clients.
    @parameterized.expand(
        [
            ("Valid Topic", "test/topic", True),
            ("Valid + Wildcard", "wild/+/card", True),
            ("Valid # Wildcard", "wild/card/#", True),
            ("Valid Non-Ascii Char", "valid/ü", True),
            ("Invalid Topic (starts with slash)", "/test/topic", False),
            ("Invalid Topic (end with slash)", "test/topic/", False),
            ("Invalid + Wildcard (not standalone)", "wild/+card", False),
            ("Invalid + Wildcard (not standalone)", "wild+/card", False),
            ("Invalid # Wildcard (not at end)", "wild/#/card", False),
            ("Invalid null character", "something\0", False),
        ],
    )
    def test_validator(self, _, topic, error):
        """Test the mqtt topic validator with multiple inputs."""
        if not error:
            with self.assertRaises(ValidationError):
                mqtt_topic_validator(topic)
        else:
            mqtt_topic_validator(topic)

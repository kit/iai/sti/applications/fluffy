"""Helper functions for permission handling."""

from projects.models import Project
from django.contrib.auth import get_user_model

User = get_user_model()


def get_pk_int_from_context(request):
    pk = request.parser_context["kwargs"].get("pk")
    return None if pk is None else int(pk)


def is_any_member_from_data(request):
    project_id = request.data.get("project")
    if project_id is None:
        return False
    return check_if_in_project(project_id, request.user)


def is_any_member_from_context(request):
    project_id = get_pk_int_from_context(request)
    if project_id is None:
        return False
    return check_if_project_member(project_id, request.user)


def check_if_in_project(project_id: int, user: User):
    return (
        check_if_project_member(project_id, user)
        or
        check_if_project_manager(project_id, user)
    )


def check_if_project_manager(project_id: int, user: User):
    """Check if provided user is manager of the provided project.

    Args:
        project_id  (int): ID of a project
        user        (User): Django User object

    Returns: True if user is manager else False
    """
    return Project.objects.get(id=project_id)\
        .managers.filter(username=user.username)\
        .exists()


def check_if_project_member(project_id: int, user: User):
    """Check if provided user is member of the provided project.

    Args:
        project_id  (int): ID of a project
        user        (User): Django User object

    Returns: True if user is member else False
    """
    return Project.objects.get(id=project_id)\
        .members.filter(username=user.username)\
        .exists()

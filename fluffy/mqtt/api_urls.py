"""Contains the api endpoints for the mqtt app."""
from rest_framework import routers
from mqtt.views import RoleViewSet, DeviceViewSet, PermissionViewSet, RootTopicViewSet
from django.urls import path, include

router = routers.SimpleRouter()
router.register("roles", viewset=RoleViewSet)
router.register("devices", viewset=DeviceViewSet)
router.register("permissions", viewset=PermissionViewSet)
router.register("root-topic", viewset=RootTopicViewSet)

urlpatterns = [
    path("", include(router.urls)),
]

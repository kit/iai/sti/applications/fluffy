from rest_framework import serializers
from mqtt.models import RootTopic
from projects.models import Project


class RootTopicSerializer(serializers.ModelSerializer):
    """Serializer for the Root Topic model."""

    project = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Project.objects.all(),
    )

    class Meta:  # noqa: WPS306
        """Metadata for the serializer."""

        model = RootTopic
        fields = ["id", "topic", "project"]

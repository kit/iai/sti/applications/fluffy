from rest_framework import serializers
from mqtt.models import Device, Role
from projects.models import Project


class DeviceSerializer(serializers.ModelSerializer):
    """Serializer for the device model."""

    roles = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Role.objects.all(),
    )
    project = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Project.objects.all(),
    )

    class Meta:  # noqa: WPS306
        """Metadata for the serializer."""

        model = Device
        fields = ["password", "id", "name", "roles", "project"]

    def validate_project(self, value):  # noqa: WPS110
        """Test if the authenticated user is member or manager of the project.

        Args:
            value (object): The project to validate

        Returns:
            value

        Raises:
            ValidationError: If user is not member or manager of the project
        """
        if self.context.get("request"):
            user = self.context["request"].user
            if (
                user in value.managers.all()
                or user in value.members.all()
                or user.is_superuser
            ):
                return value
        raise serializers.ValidationError("You are not a member of the project!")

"""Contains the serializers for the djangorestframework."""
from .device import DeviceSerializer
from .permission import PermissionSerializer
from .role import RoleSerializer
from .roottopic import RootTopicSerializer

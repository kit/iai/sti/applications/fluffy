from rest_framework import serializers
from mqtt.models import Device, Role, Permission
from mqtt.serializers import PermissionSerializer
from projects.models import Project


class RoleSerializer(serializers.ModelSerializer):
    """Serializer for the role model."""

    devices = serializers.PrimaryKeyRelatedField(
        many=True,
        queryset=Device.objects.all(),
        source="device_set",
        required=False
    )
    permissions = PermissionSerializer(
        instance=Permission.objects.all(),
        source="permission_set",
        many=True,
        required=False,
    )
    project = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Project.objects.all(),
    )

    class Meta:  # noqa: WPS306
        """Metadata for the serializer."""

        model = Role
        fields = [
            "id",
            "name",
            "description",
            "devices",
            "permissions",
            "project",
        ]

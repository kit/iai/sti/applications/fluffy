from rest_framework import serializers
from mqtt.models import Permission, Role


class PermissionSerializer(serializers.ModelSerializer):
    """Serializer for the permission model."""

    role = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Role.objects.all(),
        read_only=False
    )

    class Meta:  # noqa: WPS306
        """Metadata for the serializer."""

        model = Permission
        fields = [
            "id",
            "topic",
            "allow_publish",
            "allow_subscribe",
            "allow_qos0",
            "allow_qos1",
            "allow_qos2",
            "allow_retained",
            "allow_shared_subscriptions",
            "shared_group",
            "role",
        ]

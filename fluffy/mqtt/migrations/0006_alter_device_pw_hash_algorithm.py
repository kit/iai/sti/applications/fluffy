# Generated by Django 4.1.4 on 2023-02-15 08:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mqtt', '0005_uppercase_algorithms'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='pw_hash_algorithm',
            field=models.CharField(choices=[('MD5', 'MD5'), ('SHA512', 'SHA512'), ('BCRYPT', 'BCRYPT'), ('PKCS5S2', 'PKCS5S2'), ('PLAIN', 'plain')], max_length=10),
        ),
    ]

"""Contains object level filters for views."""
from rest_framework.filters import BaseFilterBackend
from django.db.models import Q  # noqa: WPS347


def filter_access_restricted(queryset, user, q_object):
    if user.is_superuser:
        return queryset.all()
    return queryset.filter(q_object).distinct()


class ProjectUserFilter(BaseFilterBackend):
    """Returns only models where the user is in the members or managers field."""

    def filter_queryset(self, request, queryset, view):
        """Filter the queryset.

        Args:
            request (object): django request
            queryset (object): django queryset
            view (object): The view from which the filter is called

        Returns:
            django queryset
        """
        user = request.user
        return filter_access_restricted(
            queryset,
            user,
            Q(project__managers=user) | Q(project__members=user)
        )


class PermissionListFilter(BaseFilterBackend):
    """Returns only models where the user is in the members or managers field of the related role."""

    def filter_queryset(self, request, queryset, view):
        """Filter the queryset.

        Args:
            request (object): django request
            queryset (object): django queryset
            view (object): The view from which the filter is called

        Returns:
            django queryset
        """
        user = request.user
        return filter_access_restricted(
            queryset,
            user,
            Q(role__project__managers=user) | Q(role__project__members=user)
        )

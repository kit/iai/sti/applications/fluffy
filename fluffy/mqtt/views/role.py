from django.db import IntegrityError
from rest_framework import viewsets, status
from rest_framework.response import Response

from mqtt.models import Role
from mqtt.serializers import RoleSerializer
from mqtt.filters import ProjectUserFilter
from mqtt.permissions import AllowProjectMembers

import logging
logger = logging.getLogger(__name__)


class RoleViewSet(viewsets.ModelViewSet):
    """Viewset for the role model."""

    queryset = Role.objects.all()
    serializer_class = RoleSerializer
    filter_backends = [ProjectUserFilter]
    permission_classes = [AllowProjectMembers]

    def get_queryset(self):
        project_id = self.request.query_params.get("project")
        response = (
            Role.objects.all() if project_id is None else
            Role.objects.filter(project=project_id)
        )
        return response

    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, args, kwargs)
        except IntegrityError as ex:
            return Response(ex.__str__(), status=status.HTTP_409_CONFLICT)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        try:
            result = super().destroy(request, args, kwargs)
            return result
        except Exception as ex:
            print("Error in Deleting Role", instance, request)
            return Response(
                f"Error in deleting role, not your fault: {str(ex)}",
                status=status.HTTP_500_INTERNAL_SERVER_ERROR
            )

from rest_framework import viewsets, mixins

from mqtt.models import RootTopic
from mqtt.serializers import RootTopicSerializer
from mqtt.filters import ProjectUserFilter
from mqtt.permissions import RootTopicPermissions

import logging
logger = logging.getLogger(__name__)


class RootTopicViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    """Viewset for the root topic model."""

    queryset = RootTopic.objects.all()
    serializer_class = RootTopicSerializer
    filter_backends = [ProjectUserFilter]
    permission_classes = [RootTopicPermissions]


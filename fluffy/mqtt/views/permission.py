from rest_framework import viewsets, status
from rest_framework.response import Response
from django.db.utils import IntegrityError

from fluffy.utils import force_request_data_fields
from mqtt.models import Permission, Role, RootTopic
from mqtt.serializers import PermissionSerializer
from mqtt.filters import PermissionListFilter
from mqtt.permissions import AllowRoleProjectMembers
import logging

logger = logging.getLogger(__name__)


class PermissionViewSet(viewsets.ModelViewSet):
    """Viewset for the permission model."""

    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    filter_backends = [PermissionListFilter]
    permission_classes = [AllowRoleProjectMembers]

    def get_queryset(self):
        role_id = self.request.query_params.get("role")
        if role_id is not None:
            return Permission.objects.filter(role=role_id)
        return self.queryset

    def create(self, request, *args, **kwargs):
        role = Role.objects.get(pk=request.data["role"])
        roottopic = RootTopic.objects.filter(project=role.project).first()
        topic = roottopic.topic + '/' + request.data["topic"]
        force_request_data_fields(request, {"topic": topic})
        return super().create(request, args, kwargs)

    def partial_update(self, request, *args, **kwargs):
        permission = Permission.objects.filter(pk=request.data["id"]).first()
        if permission is None:
            return Response(data="Permission does not exist anymore.", status=status.HTTP_410_GONE)
        role_id = int(request.data.get("role"))
        if role_id is not None and role_id != permission.role_id:
            raise IntegrityError("Role ID does not match the given Permission set.")
        return super().update(request, args, kwargs)

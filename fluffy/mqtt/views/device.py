from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

from fluffy.utils import get_message_from_validation_error
from mqtt.models import Device
from mqtt.serializers import DeviceSerializer
from mqtt.filters import ProjectUserFilter
from mqtt.permissions import AllowDeviceProjectMembers
from uuid import uuid4
import logging

logger = logging.getLogger(__name__)


class DeviceViewSet(viewsets.ModelViewSet):
    """Viewset for the device model."""

    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    filter_backends = [ProjectUserFilter]
    permission_classes = [AllowDeviceProjectMembers]

    def create_with_random_password(self, request):
        """
            the "password" in this context is a random and short, but not overly complicated string
            that is not personalized, but to limit device access and to be changed later on.
            therefore, it can be passed in plaintext.
        """
        password = self.create_random_password()
        updated = request.data.copy()
        updated["password"] = password
        serializer = self.get_serializer(data=updated)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response = Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        response.data["password"] = password
        return response

    def create(self, request, *args, **kwargs):
        try:
            return self.create_with_random_password(request)
        except ValidationError as ex:
            message = get_message_from_validation_error(ex)
            return Response(data=message, status=status.HTTP_409_CONFLICT)

    @staticmethod
    def create_random_password():
        return str(uuid4()).split('-')[0].upper()

    @action(methods=["PATCH"], detail=True, url_path="reset-password")
    def reset_password(self, request, pk=None):
        new_password = self.create_random_password()
        device = Device.objects.get(pk=pk)
        device.password = new_password
        device.save(force_update=True)
        return Response(data=new_password, status=status.HTTP_200_OK)

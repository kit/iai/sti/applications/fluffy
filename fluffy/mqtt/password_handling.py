"""Contains function for handling passwords."""
import base64
import os
from hashlib import sha512


def generate_salt(num_chars: int = 32) -> str:
    """Generate a pseudo random salt.

    Args:
        num_chars (int): Length of the base64 encoded string

    Returns:
        random base64 encoded string
    """
    return base64.b64encode(os.urandom(num_chars)).decode("utf-8")


def hash_password(password: str, salt: str, rounds: int, algorithm: str):
    """Password hashing as described at:
     https://www.hivemq.com/docs/ese/4.12/enterprise-security-extension/ese.html#crypto

    Args:
        password (str): Password to hash
        salt (str): Base64 encoded string for salting
        rounds (int): Rounds for hashing
        algorithm (str): Hashing Algorithm which is uses (only sha512 supported now)

    Raises:
        NotImplementedError: If hashing algorithm is not implemented

    Returns:
        Base64 Encoded string
    """
    if algorithm.casefold() == "sha512":
        hasher = sha512()
        hasher.update(salt.encode(encoding="UTF-8"))
        hasher.update(password.encode(encoding="UTF-8", errors="strict"))
        if rounds > 1:
            for r in range(rounds):
                hashed = hasher.digest()
                hasher = sha512()
                hasher.update(hashed)
        else:
            hashed = hasher.digest()
        return base64.b64encode(hashed).decode("utf-8")
    raise NotImplementedError(f"Hash algorithm {algorithm} is not supported!")

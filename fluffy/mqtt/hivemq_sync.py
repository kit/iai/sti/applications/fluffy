"""Functions to update the hivemq ese database."""
from operator import itemgetter

from django.conf import settings as django_settings
from pymysql import connect, Connection
from pymysql.cursors import Cursor, DictCursor
from typing import Optional

from mqtt.models import Role, Device, Permission, Project

import logging
logger = logging.getLogger(__name__)


def connect_hivemq(settings: Optional[dict] = None) -> Optional[Connection]:
    if settings is None:
        settings = django_settings.MQTT["HIVE_MQ_DB"]
    return connect(
        host=settings["host"],
        port=settings["port"],
        user=settings["user"],
        password=settings["password"],
        database=settings["database"],
        cursorclass=DictCursor,
    )


def update_hivemq_db():
    settings = django_settings.MQTT["HIVE_MQ_DB"]
    if settings["no_sync"] is True:
        logger.info(f"Sync HiveMQ database disabled by environment setting HIVEMQ_NO_SYNC")
        return
    db_connection = connect_hivemq(settings)
    logger.info(f"Sync HiveMQ database.")
    with db_connection:
        update_db(db_connection)


def update_db(connection: Connection, verbose: bool = False):
    """Update the hivemq ese database with data from this application."""
    loglevel = logger.level
    if verbose:
        logger.setLevel(logging.DEBUG)

    with connection.cursor() as cursor:
        _update_roles(connection, cursor, commit=False)
        _update_role_permissions_relations(connection, cursor, commit=False)
        _update_devices(connection, cursor, commit=False)
        _update_device_role_relations(connection, cursor, commit=False)
        _delete_removed_devices(connection, cursor, commit=False)
        _delete_removed_roles(connection, cursor, commit=False)
        _delete_removed_permissions(connection, cursor, commit=False)

    connection.commit()
    logger.debug("Sync HiveMQ Database finished")
    if verbose:
        logger.setLevel(loglevel)


def truncate_db(connection: Connection, verbose: bool = False):
    """Remove the role data in order to start 'fresh' - should require Admin privileges."""
    with connection.cursor() as cursor:
        if verbose:
            cursor.execute("SHOW TABLES")
            tables = cursor.fetchall()
            logger.info(f"HIVEMQ has tables: {tables}")

        cursor.execute("SET FOREIGN_KEY_CHECKS = 0")
        cursor.execute("TRUNCATE TABLE roles")
        cursor.execute("TRUNCATE TABLE role_permissions")
        cursor.execute("TRUNCATE TABLE user_roles")
        cursor.execute("SET FOREIGN_KEY_CHECKS = 1")

        if verbose:
            cursor.execute("SELECT * FROM roles")
            roles = cursor.fetchall()
            logger.info(f"Truncated roles, result is: {roles}")


def _update_role_permissions_relations(connection: Connection, cursor: Cursor, commit: bool = True):
    for permission in Permission.objects.all():
        cursor.execute(
            'SELECT role, permission FROM role_permissions WHERE role = %s AND permission = %s',
            (permission.role_id, permission.id)
        )
        existing_permission = cursor.fetchall()
        if not existing_permission:
            permission_id = _insert_permission(cursor, permission)
            _insert_role_permission_relation(
                cursor,
                permission_id,
                permission.role_id,
            )
        else:
            permission_id = existing_permission[0]["permission"]
            _update_permission(cursor, permission_id, permission)
            
    cursor.execute('SELECT * FROM role_permissions')
    rows = cursor.fetchall()
    for row in rows:
        if not Role.objects.filter(id=row["role"]).exists():
            _delete_role_permission_relation(
                cursor,
                row["permission"],
                row["role"],
            )
    if commit:
        connection.commit()


def _delete_role_permission_relation(
    cursor: Cursor,
    permission_id: int,
    role_id: int,
):
    sql = 'DELETE FROM role_permissions WHERE role = %s AND permission = %s'
    cursor.execute(sql, (role_id, permission_id))


def _upsert_role(cursor: Cursor, role: Role, name: str = None):  # requirement: role name is {project}_{name}
    if name is None:
        name = role.name
    sql = """
        INSERT INTO roles (id, name, description) VALUES (%s,%s,%s)
        ON DUPLICATE KEY UPDATE name=%s, description=%s
        """
    cursor.execute(sql, [role.id, name, role.description, name, role.description])


def _insert_role_permission_relation(
    cursor: Cursor,
    permission_id: int,
    role_id: int,
):
    sql = 'INSERT INTO role_permissions (role, permission) VALUES (%s, %s)'
    cursor.execute(sql, (role_id, permission_id))


def _update_roles(connection: Connection, cursor: Cursor, commit: bool = True):
    all_roles = list(Role.objects.all())
    for role in all_roles:
        project = Project.objects.filter(id=role.project_id)[0]
        name = f"{project.name}_{role.name}"
        _upsert_role(cursor, role, name)
    if commit:
        connection.commit()


def _delete_removed_roles(connection: Connection, cursor: Cursor, commit: bool = True):
    cursor.execute('SELECT id FROM roles')
    rows = cursor.fetchall()
    for row in rows:
        if not Role.objects.filter(id=row['id']).exists():
            _delete_role(cursor, row["id"])
    if commit:
        connection.commit()


def _delete_role(cursor: Cursor, role_id: int):
    sql = 'DELETE FROM roles WHERE id = %s'
    cursor.execute(sql, role_id)


def _delete_removed_permissions(connection: Connection, cursor: Cursor, commit: bool = True):
    cursor.execute("SELECT id FROM permissions")
    rows = cursor.fetchall()
    for row in rows:
        if not Permission.objects.filter(id=row['id']).exists():
            _delete_role_permission_relations(cursor, row['id'])
            _delete_permission(cursor, row['id'])
    if commit:
        connection.commit()


def _delete_role_permission_relations(
    cursor: Cursor,
    permission_id: int,
):
    sql = 'DELETE FROM role_permissions WHERE permission = %s'
    cursor.execute(sql, (permission_id,))


def _insert_permission(  # noqa: WPS211
    cursor: Cursor,
    permission: Permission
) -> int:
    sql = """
        INSERT INTO permissions (id, topic, publish_allowed, subscribe_allowed, qos_0_allowed,
        qos_1_allowed, qos_2_allowed, retained_msgs_allowed, shared_sub_allowed, shared_group)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """
    cursor.execute(sql, (
        permission.id,
        permission.topic,
        permission.allow_publish,
        permission.allow_subscribe,
        permission.allow_qos0,
        permission.allow_qos1,
        permission.allow_qos2,
        permission.allow_retained,
        permission.allow_shared_subscriptions,
        permission.shared_group
    ))
    return cursor.lastrowid


def _update_permission(
    cursor: Cursor,
    permission_id: int,
    permission: Permission
):
    sql = """
        UPDATE permissions SET topic=%s, publish_allowed=%s, subscribe_allowed=%s, qos_0_allowed=%s,
        qos_1_allowed=%s, qos_2_allowed=%s, retained_msgs_allowed=%s, shared_sub_allowed=%s, shared_group=%s
        WHERE id=%s
    """
    cursor.execute(sql, (
        permission.topic,
        permission.allow_publish,
        permission.allow_subscribe,
        permission.allow_qos0,
        permission.allow_qos1,
        permission.allow_qos2,
        permission.allow_retained,
        permission.allow_shared_subscriptions,
        permission.shared_group,
        permission_id
    ))


def _delete_permission(cursor: Cursor, permission_id: int):
    sql = "DELETE FROM permissions WHERE id = %s"
    cursor.execute(sql, permission_id)


def _update_devices(connection: Connection, cursor: Cursor, commit: bool = True):
    for device in Device.objects.all():
        sql = "SELECT id, username, password FROM users WHERE id = %s"
        cursor.execute(sql, device.id)
        try:
            existing_device = cursor.fetchall()[0]
            was_changed = (
                existing_device["username"] != device.name or
                existing_device["password"] != device.password
            )
            if was_changed:
                _update_device(cursor, device)
        except IndexError:
            _insert_device(cursor, device)

    if commit:
        connection.commit()


def _insert_device(cursor: Cursor, device: Device):
    cursor.execute("""
        INSERT INTO users (id, username, password, password_iterations, password_salt, algorithm)
        VALUES (%s, %s, %s, %s, %s, %s)
        """, (
            device.id,
            device.name,
            device.password,
            device.pw_hash_rounds,
            device.pw_salt,
            device.pw_hash_algorithm,
        ),
    )


def _update_device(cursor: Cursor, device: Device):
    cursor.execute("""
        UPDATE users SET username=%s, password=%s, password_iterations=%s,
        password_salt=%s, algorithm=%s
        WHERE id=%s
    """, (
        device.name,
        device.password,
        device.pw_hash_rounds,
        device.pw_salt,
        device.pw_hash_algorithm,
        device.id
    ))


def _delete_removed_devices(connection: Connection, cursor: Cursor, commit: bool = True):
    cursor.execute("SELECT id FROM users")
    rows = cursor.fetchall()
    for row in rows:
        if not Device.objects.filter(id=row["id"]).exists():
            _delete_device(cursor, row["id"])
    if commit:
        connection.commit()


def _update_device_role_relations(connection: Connection, cursor: Cursor, commit: bool = True):  # noqa: WPS210
    sql = "SELECT role_id, user_id FROM user_roles WHERE role_id = %s AND user_id = %s"
    for device in Device.objects.all():
        for role in device.roles.all():
            cursor.execute(sql, (role.id, device.id))
            existing_relation = cursor.fetchall()
            if not existing_relation:
                _insert_device_role_relation(cursor, device.id, role.id)

    cursor.execute("SELECT * FROM user_roles")
    rows = cursor.fetchall()
    for row in rows:
        user_id, role_id = itemgetter("user_id", "role_id")(row)
        is_dead = (
                not Device.objects.filter(id=user_id).exists()
                or not Device.objects.get(pk=user_id).roles.filter(pk=role_id).exists()
                or not Role.objects.filter(id=role_id).exists()
        )
        if is_dead:
            _delete_device_role_relation(cursor, user_id, role_id)
    if commit:
        connection.commit()


def _delete_device_role_relation(cursor: Cursor, user_id: int, role_id: int):
    sql = 'DELETE FROM user_roles WHERE (user_id, role_id) = (%s, %s)'
    cursor.execute(sql, (user_id, role_id))


def _delete_device(cursor: Cursor, user_id: int):
    sql = "DELETE FROM users WHERE id = %s"
    cursor.execute(sql, user_id)


def _insert_device_role_relation(cursor: Cursor, device_id: int, role_id: int):
    sql = "INSERT INTO user_roles (role_id, user_id) VALUES (%s, %s)"
    cursor.execute(sql, (role_id, device_id))

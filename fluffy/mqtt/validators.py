"""Contains validators for the mqtt model."""

from django.core.exceptions import ValidationError
import re


def mqtt_topic_validator(value):
    """Validate topic.

    Search for invalid MQTT topics.
    It was a project decision that for fluffy, all topics should start with a slash
    the MQTT specification itself leaves that open, otherwise is given by e.g.
    http://docs.oasis-open.org/mqtt/mqtt/v3.1.1/os/mqtt-v3.1.1-os.html#_Toc398718109

    Args:
        value: value to validate

    Raises:
        ValidationError: If value is not a valid mqtt topic
    """
    message = f"{value} is not a valid MQTT topic"

    if '#' in value and value[-1] != '#':
        raise ValidationError(message + ", # wildcard can only appear at the very end")

    if '\0' in value:
        raise ValidationError(message + ", null character is not allowed")

    segments = value.split('/')

    if segments[0] == '':
        raise ValidationError(message + ", must not start with slash")

    if any('+' in segment and segment != '+' for segment in segments):
        raise ValidationError(message + ", + wildcard can only appear for itself")

    if segments[-1] == '':
        raise ValidationError(message + ", must not end with slash")

    pass

"""Contains the tuples for choice fields."""

PW_HASH_ALGORITHMS = [  # noqa: WPS407
    ("MD5", "MD5"),
    ("SHA512", "SHA512"),
    ("BCRYPT", "BCRYPT"),
    ("PKCS5S2", "PKCS5S2"),
    ("PLAIN", "plain"),
]

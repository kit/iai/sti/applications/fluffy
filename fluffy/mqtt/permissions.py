"""Contains permission classes."""
from rest_framework import permissions
from projects.models import Project
from mqtt.models import Role, Permission, Device
from mqtt.permission_helpers import is_any_member_from_data, \
    is_any_member_from_context, get_pk_int_from_context, check_if_in_project


class AllowProjectMembers(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if not request.user.is_authenticated:
            return False
        if request.method in permissions.SAFE_METHODS:
            return True
        if is_any_member_from_data(request):
            return True
        if request.method == "PATCH" and is_any_member_from_context(request):
            return True
        if request.method == "DELETE":  # Bypass object related methods because these will be checked later
            return True
        return False

    def has_object_permission(self, request, view, request_object):
        if request.user.is_superuser:
            return True
        if request.user in request_object.project.managers.all():
            return True
        return request.user in request_object.project.members.all()


class AllowRoleProjectMembers(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if not request.user.is_authenticated:
            return False
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.data.get("role"):
            project_id = Role.objects.get(id=request.data.get("role")).project_id
            if check_if_in_project(project_id, request.user):
                return True
        permission_id = get_pk_int_from_context(request)
        if request.method == "PATCH" and permission_id is not None:
            project_id = Permission.objects.get(id=permission_id).role.project_id
            if check_if_in_project(project_id, request.user):
                return True
        if request.method == "DELETE":  # Bypass object related methods because these will be checked later
            return True

    def has_object_permission(self, request, view, request_object):
        if request.user.is_superuser:
            return True
        if request.user in request_object.role.project.managers.all():
            return True
        return request.user in request_object.role.project.members.all()


class RootTopicPermissions(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        if (
            request.method in permissions.SAFE_METHODS
            and request.user.is_authenticated
        ):
            return True
        if (
            request.method == "POST"
            or request.method == "PUT"
            or request.method == "PATCH"
        ) and request.data.get("project"):
            if (
                Project.objects.get(id=request.data.get("project"))
                .managers.filter(username=request.user.username)
                .exists()
            ):
                return True
        if (
            request.method == "DELETE"
        ):  # Bypass object related methods because these will be checked later
            return True

    def has_object_permission(self, request, view, request_object):
        if request.user.is_superuser:
            return True
        if request.method in permissions.SAFE_METHODS:
            return True
        if request.method == "DELETE":
            if request_object.project.managers.filter(
                username=request.user.username
            ).exists():
                return True


class AllowDeviceProjectMembers(permissions.DjangoObjectPermissions):

    def has_permission(self, request, view):
        """
        Check if the User has the permission for a non object related method.
        """
        if request.user.is_superuser:
            return True
        if not request.user.is_authenticated:
            return False
        if request.method in permissions.SAFE_METHODS:
            return True
        if is_any_member_from_data(request):
            return True
        device_id = get_pk_int_from_context(request)
        if request.method == "PATCH" and device_id is not None:
            project_id = Device.objects.get(id=device_id).project_id
            return check_if_in_project(project_id, request.user)
        if request.method == "DELETE":  # Bypass object related methods because these will be checked later
            return True
        return False

    def has_object_permission(self, request, view, request_object):
        """Check if the User has the permission for an object related method.

        Args:
            request (object): rest framework request
            view (object): The view from which the filter is called
            request_object (object): The object that should be accessed

        Returns:
            True if user is a superuser, project member or manager
        """
        if request.user.is_superuser:
            return True
        if request.user in request_object.project.managers.all():
            return True
        return request.user in request_object.project.members.all()

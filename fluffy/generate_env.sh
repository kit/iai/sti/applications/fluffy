#!/bin/bash

cd frontend-build/static

rm -f env.js
touch env.js

[ -n "$OIDC_AUTHORITY" ] && echo "window.OIDC_AUTHORITY = \""$OIDC_AUTHORITY"\";" >> env.js
[ -n "$OIDC_CLIENT_ID" ] && echo "window.OIDC_CLIENT_ID = \""$OIDC_CLIENT_ID"\";" >> env.js
[ -n "$OIDC_REDIRECT_URI" ] && echo "window.REDIRECT_URI = \""$OIDC_REDIRECT_URI"\";" >> env.js

echo "window.VERSION = \""$(grep -Po '(?<=VERSION = ).*(?=[\r\n])' ../../fluffy/__init__.py)"\";" >> env.js

# remove occurances of multiple sequential quotes if they happened to be created by accident: ""bla"" -> "bla"
sed -i -r 's/\"+([^"]*)\"+/\"\1\"/g' env.js

cd -

exit 0

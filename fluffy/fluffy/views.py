from django.shortcuts import render
from django.views.decorators.csrf import ensure_csrf_cookie
from django.utils import timezone
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView


@ensure_csrf_cookie
def index(request):
    return render(request, "index.html")


class IsRespondingEndpoint(APIView):

    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        data = {'responding': True, 'now': timezone.now()}
        return Response(data=data, status=200)

from os import getenv

from django.db import IntegrityError
from django.http import QueryDict
from rest_framework.exceptions import ValidationError
from rest_framework.request import Request


def get_message_from_validation_error(ex: ValidationError):
    return ex.args[0]['name'][0].title()


def get_message_from_integrity_error(ex: IntegrityError):
    return ex.args[0]


def getenv_bool(key, default="false"):
    return getenv(key, default=default).lower() in ('true', '1')

def getenv_int(key, default=0):
    # removes confusion about integers when possibly passed as string.
    # os.getenv() does not seem to distinguish this beforehand.
    value = getenv(key, default=default)
    try:
        return int(value.strip('"').strip("'"))
    except AttributeError:
        return int(value)

def force_request_data_fields(request: Request, update: dict):
    # depending on the source, django requests can have differing data types.
    # this can cause problems, which are likely to change with django version updates.
    # for now, the easiest way to circumvent this seems to be:
    if isinstance(request.data, QueryDict):
        request.data._mutable = True
    for key in update:
        request.data[key] = update[key]

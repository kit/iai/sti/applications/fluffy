import pymysql
from django.http import HttpResponse
from django.conf import settings
from rest_framework import status

from mqtt.hivemq_sync import update_hivemq_db

import logging
logger = logging.getLogger(__name__)


class HiveMqSyncMiddleware:

    def __init__(self, get_response):
        """Synchronize DB Mutations with HiveMQ

        Mutations of your database need to be reflected in the HiveMQ.
        This middleware synchronizes the whole databases with each request,
        which sacrifices speed for consistency - this is favourable here.
        """
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if self.could_affect_hivemq(request):
            try:
                update_hivemq_db()
            except pymysql.err.OperationalError as ex:
                # most likely: cannot access HiveMQ
                logger.error(ex)
                return HttpResponse(
                    content=ex.args[1],
                    status=status.HTTP_503_SERVICE_UNAVAILABLE
                )

        return response

    @staticmethod
    def could_affect_hivemq(request):
        if settings.HIVE_MQ_DB.get('no_sync'):
            return False
        return request.method in {"POST", "PUT", "PATCH", "DELETE"}


class SuperUserInterceptorMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.user or not settings.SUPERUSER_NAME:
            return self.get_response(request)
        if settings.SUPERUSER_NAME == request.user.username:
            request.user.is_superuser = True
        return self.get_response(request)

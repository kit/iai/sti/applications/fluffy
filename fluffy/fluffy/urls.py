"""fluffy URL Configuration."""
from django.urls import path, include
from . import views

urlpatterns = [
    path("", views.index),
    path("api/mqtt/", include("mqtt.api_urls")),
    path("api/projects/", include("projects.api_urls")),
    path("is-responding", views.IsRespondingEndpoint.as_view()),
    path("oidc/", include("oauth2_authcodeflow.urls")),
    path("", include('django_prometheus.urls')),
]

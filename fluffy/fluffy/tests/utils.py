import unittest
import os

from fluffy.utils import getenv_int


class TestEnvSetup(unittest.TestCase):
    def setUp(self):
        self.original_environ = dict(os.environ)
        os.environ.clear()

    def tearDown(self):
        os.environ.clear()
        os.environ.update(self.original_environ)

    def test_getenv_int(self):
        os.environ.update({
            "key1": "123",
            "key2": '"123"',
            "key3": "'123'",
        })
        self.assertEqual(getenv_int("key1"), 123)
        self.assertEqual(getenv_int("key2"), 123)
        self.assertEqual(getenv_int("key3"), 123)

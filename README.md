# fluffy

A self-service permissions management service for HiveMQ ESE

## Running the Backend via Docker
The application can be built via
(the following holds for PowerShell, adjust the `${PWD}` and the directory slash/backslash direction as required)
```
# after checkout, in the root directory of the project 
docker build -t fluffy .

# switch to the backend directory
cd fluffy/

# initialize the database
docker run -it --rm -p 8000:8000 --env-file .env -v ${PWD}\db:/home/fluffy/db fluffy migrate

# run the backend
docker run -it --rm -p 8000:8000 --env-file .env -v ${PWD}\db:/home/fluffy/db fluffy
```

## Running the Backend locally
For quicker checks, you can also run the backend locally
```
# after checkout, in the project root / or the backend subdirectory fluffy/
pipenv install

# in the backend subdirectory fluffy/
pipenv run python manage.py migrate

pipenv run python manage.py runserver
```

#### Running under Windows
If required to run under Windows, the `python_ldap` package has to be installed from wheel.
Look for the right system architecture and Python version under
https://github.com/cgohlke/python-ldap-build/releases
and install it
```
pipenv install .\python_ldap-3.4.4-cp311-cp311-win_amd64.whl
```
or put the corresponding line in the Pipfile (first one should be there already)
```
# python-ldap = { version = "==3.4.4", platform_system = "!= 'Windows'" }
python-ldap = { file = "./python_ldap-3.4.4-cp312-cp312-win_amd64.whl", platform_system = "== 'Windows'" }
```
and call `pipenv install` (might need to `pipenv lock` first)

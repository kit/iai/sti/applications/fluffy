#############################################################
# build the frontend
#############################################################

FROM node:20-bullseye AS frontend-build

WORKDIR /opt

COPY fluffy-frontend /opt

RUN npm -v
RUN npm ci --ignore-scripts
RUN npm run build

#############################################################
# define the python base image 
#############################################################

FROM python:3.11-bookworm AS backend-base

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
	apt-get -y install python3-dev libsasl2-dev libldap2-dev

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONFAULTHANDLER=1
ENV PYTHONUNBUFFERED=1

ENV PATH="/home/fluffy/.local/bin:$PATH"

RUN pip --version
RUN pip install -U pipenv

#############################################################
# build the backend
#############################################################

FROM backend-base AS backend-build

WORKDIR /home/fluffy

COPY Pipfile Pipfile.lock *.whl .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

#############################################################
# run the backend application (which serves the frontend)
#############################################################

FROM backend-base AS backend-run

SHELL ["/bin/bash", "-c"]

WORKDIR /home/fluffy

COPY fluffy .
COPY Pipfile .
COPY --from=backend-build /home/fluffy/.venv .venv
COPY --from=frontend-build /opt/build frontend-build

RUN echo "#!/bin/bash" > migrate
RUN echo "pipenv run python manage.py makemigrations" >> migrate
RUN echo "pipenv run python manage.py migrate" >> migrate
RUN chmod +x migrate
RUN mkdir -p .local/bin/
RUN mv migrate .local/bin/

RUN chmod +x ./generate_env.sh

EXPOSE 8000

RUN adduser fluffy
RUN chown -R fluffy:fluffy .
USER fluffy

# there are some some more environment variables (see env.example), but these are the most important ones

# is kind of arbitrary, but has to be non-empty and somewhat nonpublic (used for the user hash generation)
ENV DJANGO_SECRET_KEY=""

ENV OIDC_AUTHORITY="https://oidc.scc.kit.edu/auth/realms/kit"

# must be registered with the OpenID Provider
ENV OIDC_CLIENT_ID="iai-mqtt-ui-iai-kit-edu"

# needs to be the URL under which the frontend is available, also must be registered with the OpenID provider
ENV OIDC_REDIRECT_URI=""  

# ab1234@kit.edu or accordingly, this is the current way of determining who the (only) superuser is.
ENV SUPERUSER_NAME=""

# these are used for CSRF/CORS security, only have to be set if the application is not running under localhost:8000 inside the container
ENV HOSTNAME=""
ENV PUBLIC_PORT=8000

# set up according to your setup of the hivemq database (i.e. host for mysql db and access credentials)
ENV HIVEMQ_DB_HOST=""
ENV HIVEMQ_DB_USER=""
ENV HIVEMQ_DB_PASSWORD=""

CMD ./generate_env.sh && pipenv run python manage.py runserver 0.0.0.0:8000 --insecure

# note that --insecure sounds evil, but it allows serving static files via HTTP (not HTTPS), which is ok inside the container
